package Comparator_Iterator.Iterator;

import GenerischeDaten.Gericht;
import java.util.Iterator;

public class MyUtil {

    public static void printAll(Iterator<Gericht> ig) {
        while (ig.hasNext()) {
            System.out.println(ig.next());
        }
    }

    public static void printAll(Iterator<Gericht> ig1, Iterator<Gericht> ig2) {
        while (ig1.hasNext() && ig2.hasNext()) {
            System.out.println("1: " + ig1.next() + ", 2: " + ig2.next());
        }
    }
    // Kein überladen mit Iterator<String>, da ein Iterator <Gericht erwartet wird.

}
