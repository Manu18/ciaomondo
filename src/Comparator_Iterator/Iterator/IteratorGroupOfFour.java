package Comparator_Iterator.Iterator;

import GenerischeDaten.GroupOfFourG;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorGroupOfFour<T> implements Iterator<T> {
    private int i;
    private GroupOfFourG<T> g;

    public IteratorGroupOfFour(GroupOfFourG<T> g) {
        this.i = 0;
        this.g = g;
    }

    @Override
    public boolean hasNext() {
        return i < g.size();
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        } else {
            return g.get(++i);
        }
    }
}
