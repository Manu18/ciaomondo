package Comparator_Iterator.Iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Iterator1DArray<T> implements Iterator<T> {
    private int i;
    private int length;
    private T[] a;

    public Iterator1DArray(T[] a, int start, int ende) {
        this.a = a;
        this.i = start;
        this.length = ende;
    }

    public Iterator1DArray(T[] a) {
        this(a,0,a.length);
    }

    public Iterator1DArray(T[] a, int start) {
        this(a,start,a.length);
    }

    @Override
    public boolean hasNext() {
        return i < length;
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return a[i++];
    }

}
