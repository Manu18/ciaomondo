package Comparator_Iterator.Comparator;

import GenerischeDaten.Salat;

import java.util.Comparator;

public class ComparatorSalatWeight implements Comparator<Salat> {
    public int compare(Salat a, Salat b) {
        return a.getWeight() - b.getWeight();
    }
}
