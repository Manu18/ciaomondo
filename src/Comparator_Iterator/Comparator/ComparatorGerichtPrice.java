package Comparator_Iterator.Comparator;

import GenerischeDaten.Gericht;

import java.util.Comparator;

public class ComparatorGerichtPrice implements Comparator<Gericht> {
    public int compare(Gericht a, Gericht b) {
        /*if (a.getPrice() > b.getPrice()) {
            return 1;
        } else if( a.getPrice() == b.getPrice()) {
            return 0;
        } else { //a.getPrice() < b.getPrice()
            return -1;
        }
         */
        return a.getPrice() - b.getPrice();
    }
}
