package Comparator_Iterator.Comparator;

import GenerischeDaten.*;

import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws TableSpaceOutOfBoundsException {
        String[] array = {"hallo", "Welt", "Wie läufts", "aaaaaaaaaaaaaaa", "Hallo Welt"};
        ComparatorStringHash shc = new ComparatorStringHash();
        Comparator<String> shc2 = (a, b) -> a.length() - b.length();
        String s1 = MyUtil.max(array, shc);
        String s2 = MyUtil.max(array, shc2);
        System.out.println(s1);
        // Wie läufts
        System.out.println(s2);
        // aaaaaaaaaaaaaa

        Gericht[] array2 = {new Salat(1,2), new Pizza(1,23), new Pizza(123,21)};
        ComparatorGerichtPrice comp1 = new ComparatorGerichtPrice();
        Comparator<Gericht> comp2 = (gericht, gericht2) -> gericht.getCourse().length() - gericht2.getCourse().length();
        System.out.println(MyUtil.max(array2,comp1));
        // Pizza mit Duchmesser von 21
        System.out.println(MyUtil.max(array2,comp2));
        // Salat mit Gewicht von 2

        GroupOfFourG<Gericht> g4 = new GroupOfFourG<Gericht>();
        g4.appendLast(new Pizza(1,2));
        g4.appendLast(new Salat(2,23));
        g4.appendLast(new Pizza(3,212));
        g4.appendLast(new Pizza(4,2123));
        GroupIFG<Gericht> gi = (GroupIFG<Gericht>) g4;

        System.out.println(MyUtil.max(gi, comp2));

        Salat[] salats = new Salat[1];
        salats[0] = new Salat(12,12);

        MyUtil.max(salats, comp2);

        /* Compilerfehler bei:
        Comparator<Gericht>, - -
        Comparator<Salat>, 2 3
        Comparator<Pizza>, 2 3
        Gericht[], - -
        Salat[], - -
        Pizza[], - -
        GroupIF<Gericht>, 2 -
        GroupIF<Salat>, 2 3
        GroupIF<Pizza> 2 3
        */
    }
}
