package Comparator_Iterator.Comparator;

import GenerischeDaten.Pizza;

import java.util.Comparator;

public class ComparatorPizzaDiameter implements Comparator<Pizza> {
    public int compare(Pizza a, Pizza b) {
        return a.getDiameter() - b.getDiameter();
    }
}
