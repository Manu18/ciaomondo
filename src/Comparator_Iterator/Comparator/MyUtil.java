package Comparator_Iterator.Comparator;

import GenerischeDaten.GroupIFG;
import GenerischeDaten.Gericht;

import java.util.Comparator;

public class MyUtil {
    public static String max(String[] array, Comparator<String> comperator) {
        String erg = new String();
        erg = array[0];
        for (int i = 1; i < array.length; i++) {
            if (comperator.compare(erg, array[i]) < 0) {
                erg = array[i];
            }
        }
        return erg;
    }

    public static Gericht max(Gericht[] array, Comparator<Gericht> comperator) {
        Gericht erg = array[0];
        for (int i = 1; i < array.length; i++) {
            if (comperator.compare(erg, array[i]) < 0 ) {
                erg = array[i];
            }
        }
        return erg;
    }

    public static Gericht max(GroupIFG<Gericht> gi, Comparator<Gericht> comperator) {
        int length = gi.size();
        Gericht[] garray = new Gericht[length];
        for (int i = 0; i < length; i++) {
            garray[i] = gi.removeLast();
        }
        if (length > 0) {
            return max(garray, comperator);
        } else {
            return null;
        }
    }

        /* Compilerfehler bei:
        Comparator<Gericht>, - -
        Comparator<Salat>, 2 3
        Comparator<Pizza>, 2 3
        Gericht[], - -
        Salat[], - -
        Pizza[], - -
        GroupIF<Gericht>, 2 -
        GroupIF<Salat>, 2 3
        GroupIF<Pizza> 2 3
        */
}
