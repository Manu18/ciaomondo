package Comparator_Iterator.Comparator;

import java.util.Comparator;

public class ComparatorStringHash implements Comparator<String> {
    public int compare(String a, String b) {
        return a.hashCode() - b.hashCode();
        //Es gibt nur eine begrenzte Anzahl Hashwerte => Hashes können für ungleiche Objekte gleich sein
        //Bezogen auf das Beispiel: Es kommt zu einem Über-/Unterlauf bei der Integer-Operation => Beziehung falsch herum ausgegeben
    }
}
