package neu.Iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Iterator1DArray<E> implements Iterator<E> {
    private int i;
    private int length;
    private E[] a;

    public Iterator1DArray(E[] a) {
        this(a,0,a.length);
    }
    public Iterator1DArray(E[] a, int start, int ende) {
        i = start;
        if (start > length | ende > a.length) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public boolean hasNext() {
        return i < length;
    }

    @Override
    public E next() {
        if (hasNext()) {
            return a[i++];
        } else {
            throw new NoSuchElementException();
        }
    }
}
