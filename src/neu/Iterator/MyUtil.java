package neu.Iterator;

import neu.junit.Gericht;

import java.util.Iterator;

public class MyUtil {

    public static void printAll(Iterator<Gericht> iter) {
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

    public static void printAll(Iterator<Gericht> iter1, Iterator<Gericht> iter2) {
        while (iter1.hasNext() && iter2.hasNext()) {
            System.out.println("1: " + iter1.next() + ", 2: " + iter2.next());
        }
    }
}
