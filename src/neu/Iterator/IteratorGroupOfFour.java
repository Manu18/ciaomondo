package neu.Iterator;

import Comparable_Iteratable.Iteratable.GroupOfFour;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorGroupOfFour<T> implements Iterator<T> {
    private GroupOfFour<T> group;
    private int i;

    public IteratorGroupOfFour(GroupOfFour g) {
        group = g;
    }

    @Override
    public boolean hasNext() {
        return i < group.size();
    }

    @Override
    public T next() {
        if (hasNext()) {
            return group.get(i++);
        } else {
            throw new NoSuchElementException();
        }
    }
}
