package neu.junit;

public class GroupOfFour {
    private int size = 0;
    protected Gericht ersterPlatz;
    protected Gericht zweiterPlatz;
    private Gericht dritterPlatz;
    private Gericht vierterPlatz;

    public GroupOfFour() {

    }
    public int size() {
        return size;
    }
    public boolean isEmpty() {
        return size == 0;
    }

    public void appendLast(Gericht g) throws TableSpaceOutOfBoundsException {
        if (size < 4) {
            switch (size) {
                case 0:
                    ersterPlatz = g;
                    size++;
                    break;
                case 1:
                    zweiterPlatz = g;
                    size++;
                    break;
                case 2:
                    dritterPlatz = g;
                    size++;
                    break;
                case 3:
                    vierterPlatz = g;
                    size++;
                    break;
            }
        } else {
            throw new TableSpaceOutOfBoundsException();
        }
    }

    public Gericht removeLast() {
        Gericht r = null;
        if (size > 0) {
            switch (size) {
                case 1:
                    r = ersterPlatz;
                    ersterPlatz = null;
                    size--;
                    break;
                case 2:
                    r= zweiterPlatz;
                    zweiterPlatz = null;
                    size--;
                    break;
                case 3:
                    r= dritterPlatz;
                    dritterPlatz = null;
                    size--;
                    break;
                case 4:
                    r = vierterPlatz;
                    vierterPlatz = null;
                    size--;
                    break;
            }
        } else {
            throw new NullPointerException();
        }
        return r;
    }
    public Gericht get(int position) {
        if (position > 0 && position <= 4) {
            switch (position) {
                case 1:
                    return ersterPlatz;
                case 2:
                    return zweiterPlatz;
                case 3:
                    return dritterPlatz;
                case 4:
                    return vierterPlatz;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
        return null;
    }
    public void swap(int i1, int i2) {
        Gericht c1 = this.get(i1);
        Gericht c2 = this.get(i2);

        if (c1 == null | c2 == null) {
            throw new NullPointerException("Swap mit leerem Objekt nicht möglich");
        }

        switch (i1) {
            case 1:
                this.ersterPlatz = c2;
                break;
            case 2:
                this.zweiterPlatz = c2;
                break;
            case 3:
                this.dritterPlatz = c2;
                break;
            case 4:
                this.vierterPlatz= c2;
                break;
        }
        switch (i2) {
            case 1:
                this.ersterPlatz = c1;
                break;
            case 2:
                this.zweiterPlatz = c1;
                break;
            case 3:
                this.dritterPlatz = c1;
                break;
            case 4:
                this.vierterPlatz = c1;
                break;
        }
    }


    public void clear() {
        ersterPlatz = null;
        zweiterPlatz = null;
        dritterPlatz = null;
        vierterPlatz = null;
    }
    public String toString() {
        String s = "Tisch mit " + size() + " Personen: \n";
        for (int i = 0; i < size; i++) {
            s += "Fuer den " + i + ". Platz: " + get(i).toString() + "\n";
        }
        return s;
    }



}
