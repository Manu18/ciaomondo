package neu.junit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class GoFTest {
    GroupOfFour group;
    Gericht testgericht1;
    Gericht testgericht2;
    Gericht testgericht3;
    Gericht testgericht4;
    Gericht[] testgerichte = new Gericht[4];


    @BeforeEach
    void setup() throws TableSpaceOutOfBoundsException {
        this.group = new GroupOfFour();
        this.testgericht1 = new Pizza(1,20);
        this.testgericht2 = new Pizza(2,20);
        this.testgericht3 = new Pizza(3,20);
        this.testgericht4 = new Pizza(4,20);
        testgerichte[0] = testgericht1;
        testgerichte[1] = testgericht2;
        testgerichte[2] = testgericht3;
        testgerichte[3] = testgericht4;
        group.appendLast(testgericht1);
        group.appendLast(testgericht2);
        group.appendLast(testgericht3);
        group.appendLast(testgericht4);
    }
    @Test
    void test() {
        assertEquals(testgericht1, group.get(1), "Falsche Position");
        assertEquals(testgericht2, group.get(2), "Falsche Position");
        assertEquals(testgericht3, group.get(3), "Falsche Position");
        assertEquals(testgericht4, group.get(4), "Falsche Position");

        assertThrows(TableSpaceOutOfBoundsException.class, () -> group.appendLast(testgericht1), "Keine Fehlermeldung");

        assertEquals(4,group.size(), "Falsche Groesse");
        assertEquals(testgericht4, group.removeLast(), "Falsches Gericht");
        group.swap(1, 2);
        assertEquals(testgericht1, group.get(2), "Nicht getauscht");
        assertEquals(testgericht2, group.get(1), "Nicht getauscht");
    }

    // swap mit leerem Objekt
    @Test
    void swapEmptyTest() throws TableSpaceOutOfBoundsException {
        group.clear();
        assertThrows(NullPointerException.class, () -> group.swap(1,3), "Swap Fehler mit leerem Element Fehler nicht erzeugt");
    }

    // get ausserhalb vom Index
    @Test
    void getIntdexTest() {
        assertThrows(IndexOutOfBoundsException.class, () -> group.get(5), "Get Index Fehler wurde nicht erzeugt");
        assertThrows(IndexOutOfBoundsException.class, () -> group.get(0), "Get Index Fehler wurde nicht erzeugt");
    }

}
