package neu.junit;

public class Salat extends Gericht{
    private int weight = 0;

    public Salat() {
        super();
    }
    public Salat(int p, int w) {
        super("Salat",p);
        setWeight(w);
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int w) {
        weight = w;
    }
    public String toString() {
        String s = super.toString() + " mit einem Gewicht von " + getWeight();
        return s;
    }
}
