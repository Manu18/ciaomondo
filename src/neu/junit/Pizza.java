package neu.junit;

public class Pizza extends Gericht{
    private int diameter = 0;

    public Pizza() {
        super();
    }
    public Pizza(int p, int d) {
        super("Pizza", p);
        setDiameter(d);
    }
    public int getDiameter() {
        return diameter;
    }
    public void setDiameter(int d) {
        diameter = d;
    }
    public String toString() {
        String s = super.toString() + " mit einem Durchmesser von " + getDiameter();
        return s;
    }
}
