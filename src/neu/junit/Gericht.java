package neu.junit;

public abstract class Gericht {
    private String course = "";
    private int price = 0;

    public Gericht() {
    }
    public Gericht(String s, int i) {
        course = s;
        price = i;
    }
    public String getCourse() {
        return course;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        String s = this.getCourse() + " fuer den Preis von " + getPrice() + " Euro";
        return s;
    }

}
