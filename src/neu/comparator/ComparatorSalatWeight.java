package neu.comparator;

import java.util.Comparator;
import neu.junit.Salat;

public class ComparatorSalatWeight implements Comparator<Salat> {
    @Override
    public int compare(Salat o1, Salat o2) {
        return o1.getWeight() - o2.getWeight();
    }
}
