package neu.comparator;

import java.util.Comparator;
import neu.junit.Gericht;

public class ComparatorGerichtPrice implements Comparator<Gericht> {
    @Override
    public int compare(Gericht o1, Gericht o2) {
        return o1.getPrice() - o2.getPrice();
    }
}
