package neu.comparator;

import java.util.Comparator;
import neu.junit.Pizza;

public class ComparatorPizzaDiameter implements Comparator<Pizza> {
    @Override
    public int compare(Pizza o1, Pizza o2) {
        return o1.getDiameter() - o2.getDiameter();
    }
}
