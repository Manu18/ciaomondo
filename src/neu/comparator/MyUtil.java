package neu.comparator;

import neu.junit.GroupIF;
import neu.junit.Gericht;

import java.util.Comparator;

public class MyUtil {

    public static String max(String[] a, Comparator<String> comp) {
        int k = 0;
        for (int i = 1; i < a.length; i++) {
            if (comp.compare(a[i],a[k]) > 0) {
                k = i;
            }
        }
        return a[k];
    }

    public static Gericht max(Gericht[] a, Comparator<Gericht> comp) {
        int k = 0;
        for (int i = 1; i < a.length; i++) {
            if (comp.compare(a[i],a[k]) > 0) {
                k = i;
            }
        }
        return a[k];
    }

    public static Gericht max(GroupIF a, Comparator<Gericht> comp) {
        int k = 0;
        for (int i = 1; i < a.size(); i++) {
            if (comp.compare(a.get(i),a.get(k)) > 0) {
                k = i;
            }
        }
        return a.get(k);
    }
}
