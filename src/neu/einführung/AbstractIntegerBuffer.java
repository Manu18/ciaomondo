package neu.einführung;

public abstract class AbstractIntegerBuffer implements IntegerBuffer {
    protected int size = 0;
    protected Integer[] array;

    @Override
    public void push(Integer i) {
        if (capacity() <= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (size < array.length) {
            array[size++] = i;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

}
