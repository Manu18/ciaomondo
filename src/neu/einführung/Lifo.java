package neu.einführung;

import java.util.Random;

public class Lifo extends AbstractIntegerBuffer {
    private Integer array[];
    private int size;

    public Lifo(int s) {
        if (s <= 0) {
            throw new  IllegalArgumentException();
        }
        array = new Integer[s];
        size = 0;
    }
    public Lifo() {
        Random r = new Random();
        int s;
        do {
            s = r.nextInt(100);
        } while (s <= 3);
        array = new Integer[s];
        size = 0;
    }

    public Integer pop() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Integer c = array[size--];
        return c;
    }

}
