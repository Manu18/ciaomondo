package neu.einführung;

import java.util.Random;

public class Fifo extends AbstractIntegerBuffer {
    private Integer array[];
    private int size;

    public Fifo(int s) {
        if (s <= 0) {
            throw new  IllegalArgumentException();
        }
        array = new Integer[s];
        size = 0;
    }
    public Fifo() {
        Random r = new Random();
        int s;
        do {
            s = r.nextInt(100);
        } while (s <= 3);
        array = new Integer[s];
        size = 0;
    }

    public Integer pop() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Integer c = array[0];
        for (int i = 0; i < size; i++) {
            array[i] = array[i+1];
        }
        size--;
        return c;
    }

}
