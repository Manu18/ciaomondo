package neu.einführung;

public interface IntegerBuffer {
    void push(Integer i);
    Integer pop();
    int size();
    int capacity();
}
