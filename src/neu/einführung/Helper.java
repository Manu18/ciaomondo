package neu.einführung;

public class Helper {
    public static void move(IntegerBuffer quelle, IntegerBuffer ziel) {
        while (quelle.size() > 0 && ziel.capacity() > ziel.size()) {
            ziel.push(quelle.pop());
        }
    }
}
