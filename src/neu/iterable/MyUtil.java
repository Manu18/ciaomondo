package neu.iterable;

import neu.junit.Gericht;
import neu.junit.Pizza;
import neu.junit.Salat;
import neu.junit.TableSpaceOutOfBoundsException;

public class MyUtil {

    public static <T> void printAll(GroupOfFourG<T> iter) {
        for(T e : iter) {
            System.out.println(e);
        }
    }

    public static <T> void printAll(GroupOfFourG<T> iter1, GroupOfFourG<T> iter2) {
        for(T e : iter1) {
            System.out.println("1: " + e);
        }
        for(T e : iter2) {
            System.out.println("2: " + e);
        }
    }

    public static <T> void shallowCopy(GroupOfFourG<T> q, GroupOfFourG<T> z) throws TableSpaceOutOfBoundsException {
        if (z.size() != 0) {
            throw new TableSpaceOutOfBoundsException();
        } else {
            for (T e: q) {
                z.appendLast(e);
            }
        }
    }

    public static void main(String[] args) throws TableSpaceOutOfBoundsException {
        // A1
        /*
            foreach hat nur lesenden Zugriff, dh. funktioniert es so nicht
         */

        GroupOfFourG<Gericht> quelle = new GroupOfFourG<>();
        GroupOfFourG<Gericht> ziel = new GroupOfFourG<>();
        Gericht g1 = new Pizza(12,12);
        Gericht g2 = new Pizza(23,2);
        Gericht g3 = new Pizza(345,32);
        Salat s1 = new Salat(10,123);
        Salat s2 = new Salat(10,123);
        Salat s3 = new Salat(10,123);
        quelle.appendLast(g1);
        quelle.appendLast(g2);
        quelle.appendLast(g3);
        quelle.appendLast(g3);

        printAll(quelle);
        shallowCopy(quelle,ziel);
        printAll(ziel);
        /*
            Eine deepcopy ist nicht möglich, da sich die konkreten Typen unterscheiden können (beispielsweise kann eine GOF<Gericht>
            Salatelemente enthalten) und das Instanzieren der neuen Objekte ist somit ohne weitere Logik nicht möglich.
         */
    }
}
