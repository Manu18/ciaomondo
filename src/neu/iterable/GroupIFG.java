package neu.iterable;

import neu.junit.TableSpaceOutOfBoundsException;

import java.util.Iterator;

public interface GroupIFG<T> extends Iterable<T> {
    int size();
    boolean isEmpty();
    void appendLast(T g) throws TableSpaceOutOfBoundsException;
    T removeLast();
    T get(int p);
    void swap(int p1, int p2);
    Iterator<T> iterator();
}
