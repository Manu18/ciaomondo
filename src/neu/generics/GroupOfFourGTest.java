package neu.generics;

import neu.junit.Gericht;
import neu.junit.Pizza;
import neu.junit.Salat;
import neu.junit.TableSpaceOutOfBoundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GroupOfFourGTest {
    String s1, s2, s3, s4;
    Pizza p1, p2, p3, p4;
    Salat sa1, sa2, sa3, sa4;

    @BeforeEach
    void setup() {
        s1 = "a";
        s2 = "b";
        s3 = "c";
        s4 = "d";

        p1 = new Pizza(1,1);
        p2 = new Pizza(2,1);
        p3 = new Pizza(3,1);
        p4 = new Pizza(4,1);

        sa1 = new Salat(1,1);
        sa2 = new Salat(2,1);
        sa3 = new Salat(3,1);
        sa4 = new Salat(4  ,1);
    }

    @Test
    void appendLast() throws TableSpaceOutOfBoundsException {
        GroupOfFourG<String> g = new GroupOfFourG<>();
        g.appendLast(s1);
        assertEquals(s1, g.removeLast(), "Falsch eingefügt");
        assertEquals(0, g.size(), "falsche größe");
        g.appendLast(s1);
        g.appendLast(s2);
        g.appendLast(s3);
        g.appendLast(s4);
        assertEquals(s4, g.removeLast(), "falsch eingefügt");
        g.appendLast(s4);
        assertThrows(TableSpaceOutOfBoundsException.class, () -> g.appendLast(s4), "Fehler");
        assertEquals(4, g.size(), "Falsche größe");
        assertEquals(s1, g.get(1), "Falsche position");
        g.swap(1,2);
        assertEquals(s2, g.get(1), "Falsche position");
    }

    @Test
    void typeTest() throws TableSpaceOutOfBoundsException {
        GroupOfFourG<Pizza> g = new GroupOfFourG<>();
        // g.appendLast(sa1); Fehler
        GroupOfFourG<Gericht> g2 = new GroupOfFourG<>();
        g2.appendLast(sa1); // kein Problem
        Salat sa5 = (Salat) g2.removeLast(); // Cast notwendig
        g2.appendLast(p1);
        Pizza p5 = (Pizza) g2.removeLast(); // Cast notwendig
    }

    @Test
    void constructorTests() throws GenerischeDaten.TableSpaceOutOfBoundsException, TableSpaceOutOfBoundsException {
        Pizza[] pizzas = {p1};
        GroupOfFourG<Pizza> testg1 = new GroupOfFourG<>(pizzas);
        assertEquals(1, testg1.size(), "Konstruktor funktioniert nicht wie erwartet");
        assertEquals(p1.getPrice(), testg1.removeLast().getPrice(), "Konstruktor funktioniert nicht wie erwartet");
    }
}
