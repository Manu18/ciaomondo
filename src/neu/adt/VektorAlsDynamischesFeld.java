package neu.adt;

import java.util.Comparator;

public class VektorAlsDynamischesFeld<T> implements Vektor<T> {
    private T[] a;
    private int size;

    public VektorAlsDynamischesFeld(int max) {
        a = (T[]) new Object[max];
        size = 0;
    }

    public boolean add(int index, T t) {
        if (index >= 0 && index < a.length) {
            size = index + 1;
            a[index] = t;
            return true;
        } else if (index >= a.length) {
            // Feld vergrößern
            T[] n = (T[]) new Object[a.length * 2];
            for (int i = 0; i < size; i++) {
                n[i] = a[i];
            }
            a = n;
            size = index + 1;
            a[index] = t;
            return true;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public boolean add(T t) {
        return add(size, t);
    }

    public T get(int index) {
        if (index >= 0 && index < size) {
            return a[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public T remove(int index) {
        if (index > 0 && index < size) {
            T cache = a[index];
            a[index] = null;
            if (size - 1 == index) {
                size--;
            }
            if (size < (a.length / 4)) {
                // verkleinern
                T[] n = (T[]) new Object[a.length / 2];
                for (int i = 0; i < size; i++) {
                    n[i] = a[i];
                }
                a = n;
            }
            return cache;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public boolean isEmpty() {
        return (0 == size);
    }

    public int size() {
        return size;
    }

    public int maxSize() {
        return a.length;
    }

    public void sort(Comparator<? super T> c) {
        int sortiert = 0;
        while (sortiert < (size - 1)) {
            sortiert = 0;
            for (int i = 0; i < (size - 1); i++) {
                if (c.compare(a[i], a[i+1]) < 0) {
                    T cache = a[i];
                    a[i] = a[i+1];
                    a[i+1] = cache;
                    sortiert = 0;
                } else {
                    sortiert++;
                }
            }
        }
    }
}
