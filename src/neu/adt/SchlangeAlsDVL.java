package neu.adt;

import java.util.Comparator;

public class SchlangeAlsDVL<E> implements Schlange<E> {
    private int size = 0;
    LE first = null;
    LE last = null;

    class LE {
        E data;
        LE next;
        LE prev;

        LE (E t) {
           data = t;
           next = null;
           prev = null;
        }
    }

    public boolean add(E e) {
        // hinten anfügen
        LE l = new LE(e);
        if (size == 0) {
            first = l;
            last = l;
            size++;
            return true;
        }

        last.next = l;
        l.prev = last;
        last = l;

        size++;
        return true;
    }

    public boolean remove(E e) {
        LE l = first;
        if (first.data.equals(e)) {
            first = first.next;
            first.prev = null;
        }
        if (last.data.equals(e)) {
            last = last.prev;
            last.next = null;
        }


        for (int i = 0; i < size; i++) {
            if (l.data.equals(e)) {
                l.prev.next = l.next;
                l.next.prev = l.prev;
                size--;
                return true;
            } else {
                l = l.next;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public E remove() {
        //LIFO
        E l = this.last.data;
        last = last.prev;
        size--;
        return l;
    }

    public E peek() {
        // oder auch first.data
        return last.data;
    }

    public void sort(Comparator<? super E> comp) {
        LE a = this.first;
        LE temp = null;
        int j = 0;
        while (j < size) {
            temp = a;
            for (int i = j; i < size - 1; i++) {
                temp = temp.next;
                if (comp.compare(a.data, temp.data) > 0) {
                    E cache = temp.data;
                    temp.data = a.data;
                    a.data = cache;
                }
            }
            a = a.next;
            j++;
        }
    }
}
