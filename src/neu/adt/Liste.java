package neu.adt;

import java.util.Comparator;

public interface Liste<T> {
    boolean add(int index, T t);
    T get(int index); // throws IndexOutOfBoundsException
    T remove(int index); // throws IndexOutOfBoundsException

    // optional fuer doppelt verkettete Listen:
    // T peek(); // throws NoSuchElementException

    // optional
    void sort(Comparator<? super T> c);
}
