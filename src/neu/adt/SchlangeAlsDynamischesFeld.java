package neu.adt;

public class SchlangeAlsDynamischesFeld<T> implements Schlange<T> {
    private T[] a;
    private int size;
    private int c;

    public SchlangeAlsDynamischesFeld(int max) {
        a = (T[]) new Object[max];
        size = 0;
    }

    public boolean add(T t) {
        int index = size;
        if (index >= 0 && index < a.length) {
            size++;
            a[index] = t;
            return true;
        } else if (index >= a.length) {
            // Feld vergrößern
            T[] n = (T[]) new Object[a.length * 2];
            for (int i = 0; i < size; i++) {
                n[i] = a[i];
            }
            a = n;
            size = index + 1;
            a[index] = t;
            return true;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public boolean remove(T t) {
        for (int i = 0; i < size; i++) {
            if (t.equals(a[i])) {
                a[i] = null;
                if (size - 1 == i) {
                    size--;
                }
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }
    public int size() {
        return size;
    }

    public T peek() {
        return a[c++];
    }

    public T remove() {
        T cache = a[c];
        a[c] = null;

        return cache;
    }
}
