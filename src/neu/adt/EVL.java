package neu.adt;

public class EVL <T> {
     private class Item {
         T element;
         Item next;

         Item(T e) {
             element = e;
             next = null;
         }
     }
     private Item first = null;

     public void append(T e) {
         // fügt am Ende der Liste ein Item mit dem element e ein
         Item element = new Item(e);
         if (first == null) {
            first = element;
         } else  {
             Item cache = first;
             while (cache.next != null) {
                 cache = cache.next;
             }
             cache.next = element;
         }
     }

     public T get(int p) {
         Item tmp = first;
         while ((tmp != null) && (p-- > 0)) tmp = tmp.next;
         if (null != tmp) return tmp.element;
         else return null;
     }


    public static EVL<Paar> paarliste(EVL l1, EVL l2) {
         EVL<Paar> e = new EVL<>();
         int i = 0;
         while (l1.get(i) != null && l2.get(i) != null) {
             Paar p = new Paar(l1.get(i), l2.get(i));
             e.append(p);
             i++;
         }
         return e;
    }

    public static void main(String[] args) {
        EVL<String> l1 = new EVL<>();
        l1.append("Heute");
        l1.append("hier");
        l1.append("morgen");
        l1.append("dort");

        EVL<String> l2 = new EVL<>();
        l2.append("Today");
        l2.append("here");
        l2.append("tomorrow");
        l2.append("somewhere");
        l2.append("else");
        //EVL l1 = {"Heute", "hier", "morgen", "dort"};
        //EVL l2 = {"Today", "here", "tomorrow", "somewhere", "else"};
        // paarliste(l1,l2) => {[Heute:Today],[hier:here],[morgen:tomorrow],[dort:somewhere]}
        EVL<Paar> paarEVL = paarliste(l1,l2);
        int p = 0;
        while (paarEVL.get(p) != null) {
            System.out.println(paarEVL.get(p).erstes() + " : " + paarEVL.get(p).zweites());
            p++;
        }
    }
}
