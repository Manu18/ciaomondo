package neu.adt;

public interface Schlange<T> extends Kollektion<T> {
    boolean add(T t);
    T remove(); // throws NoSuchElement
    T peek();
}
