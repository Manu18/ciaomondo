package neu.adt;

import Abstrakte_Daten_Typen.Alphabetic_Comparator;

import java.util.Comparator;

public class ListeAlsFeld<T> implements Liste<T> {
    private int size;
    private T data[];

    @SuppressWarnings("unchecked")
    public ListeAlsFeld(int max) {
        size = 0;
        data = (T[]) new Object[max];
    }
    public boolean add(int index, T t) {
        if (index < size) {
            // ueberschreiben
            data[index] = t;
        } else if (index < data.length) {
            data[index] = t;
            size = index + 1;
        } else {
            throw new IndexOutOfBoundsException();
        }
        return true;
    }

    public T get(int index) {
        if (index < data.length) {
            if (index < size) {
                return data[index];
            } else {
                throw new NullPointerException();
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public T remove(int index) {
        T cache = data[index];
        data[index] = null;
        if (index == (size - 1)) {
            size--;
        }
        return cache;
    }

    public boolean add(T t) {
        return add(size, t);
    }

    public boolean remove(T t) {
        for (int i = 0; i < size; i++) {
            if (t.equals(data[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return (0 == size);
    }

    public int size() {
        return size;
    }

    // optional
    public void sort(Comparator<? super T> c) {
        int sortiert = 0;
        while (sortiert < (size - 1)) {
            sortiert = 0;
            for (int i = 0; i < (size - 1); i++) {
                if (c.compare(data[i], data[i+1]) < 0) {
                    T cache = data[i];
                    data[i] = data[i+1];
                    data[i+1] = cache;
                    sortiert = 0;
                } else {
                    sortiert++;
                }
            }
        }
    }
}
