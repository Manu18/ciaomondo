package neu.adt;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

class DVLSchlangeTest {
    private SchlangeAlsDVL<String> t;

    @BeforeEach
    void setUp() {
        t = new SchlangeAlsDVL<>();
        t.add("a");
        t.add("e");
        t.add("b");
        t.add("d");
        t.add("c");
    }

    @AfterEach
    public void tearDown() {
        t = null;
    }

    @Test
    void testadd() {
        assertEquals("c", t.peek(),"Falsche peek");
        t.add("g");
        assertEquals(6, t.size(),"Falsche Größe");
        assertEquals("g",t.remove(),"Falsche Rückgabe");
        assertEquals(5, t.size(),"Falsche Größe");
    }

    @Test
    public void testsort() {
        StringComparator sc = new StringComparator();
        t.add("f");
        t.sort(sc);
        String[] abc = {"a","b","c","d","e","f","g"};
        int k = t.size();
        for (int i = 0; i < k; i++) {
            assertEquals(abc[i], t.remove(), "nicht sortiert");
        }
    }
}
