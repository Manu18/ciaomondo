package neu.adt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ListSortTest {
    StringComparator sc = null;
    ListeAlsFeld<String> liste = null;

    @BeforeEach
    void setup() {
        sc = new StringComparator();
        liste = new ListeAlsFeld<>(10);
    }

    @Test
    void test() {
        liste.add("a");
        liste.add("e");
        liste.add("b");
        liste.add("d");

        assertEquals("a", liste.get(0), "Fehler get");
        assertEquals("d", liste.get(3), "Fehler get");
        assertThrows(IndexOutOfBoundsException.class, () -> liste.get(40), "Fehler OutOfBounds");

        liste.add("c");
        liste.sort(sc);
        String[] abc = {"a","b","c","d","e"};
        for (int i = 0; i < liste.size(); i++) {
            assertEquals(abc[i], liste.get(i), "Falsch sortiert");
        }


    }
}
