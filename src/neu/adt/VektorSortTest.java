package neu.adt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VektorSortTest {
    StringComparator sc = null;
    VektorAlsDynamischesFeld<String> vektor = null;

    @BeforeEach
    void setup() {
        sc = new StringComparator();
        vektor = new VektorAlsDynamischesFeld<>(5);
    }

    @Test
    void test() {
        vektor.add("a");
        vektor.add("e");
        vektor.add("b");
        vektor.add("d");
        vektor.add("c");

        assertEquals("a", vektor.get(0), "Fehler get");
        assertEquals("d", vektor.get(3), "Fehler get");
        assertThrows(IndexOutOfBoundsException.class, () -> vektor.get(40), "Fehler OutOfBounds");

        assertEquals(5, vektor.maxSize(),"Falsche Größe");
        // größe verdoppeln
        vektor.add("f");
        assertEquals(10, vektor.maxSize(),"Falsche Größe");

        vektor.sort(sc);
        String[] abc = {"a","b","c","d","e","f"};
        for (int i = 0; i < vektor.size(); i++) {
            assertEquals(abc[i], vektor.get(i), "Falsch sortiert");
        }


    }
}
