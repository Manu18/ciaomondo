package neu.comparable;

import Comparable_Iteratable.Comparable.Pizza;
import neu.junit.Gericht;

public class Salat extends Gericht implements Comparable<Salat> {
    private int weight = 0;

    public Salat() {
        super("1 - Vorspeise",5);
    }
    public Salat(int p, int w) {
        super("1 - Vorspeise",p);
        setWeight(w);
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int w) {
        weight = w;
    }
    public String toString() {
        String s = super.toString() + " mit einem Gewicht von " + getWeight();
        return s;
    }

    public int compareTo(Salat t) {
        int c = this.getWeight() - t.getWeight();
        return c;
    }

}
