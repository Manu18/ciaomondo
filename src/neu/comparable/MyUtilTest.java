package neu.comparable;
import neu.comparator.ComparatorGerichtPrice;
import neu.comparator.ComparatorPizzaDiameter;
import neu.comparator.ComparatorStringHash;
import neu.generics.GroupIFG;
import neu.generics.GroupOfFourG;
import neu.junit.TableSpaceOutOfBoundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyUtilTest {
    GroupIFG<String> gs;
    GroupIFG<Pizza> gp;
    String s1, s2, s3;
    Pizza p1, p2, p3, p4;
    Comparator<String> s;
    ComparatorStringHash sh;
    Comparator<Pizza> pd;

    @BeforeEach
    void setup() throws TableSpaceOutOfBoundsException {
        gs = new GroupOfFourG<>();
        gp = new GroupOfFourG<>();
        s1 = "a";
        s2 = "b";
        s3 = "c";
        p1 = new Pizza(1, 2);
        p2 = new Pizza(2, 3);
        p3 = new Pizza(3, 5);
        p4 = new Pizza(4, 8);
        gs.appendLast(s1);
        gs.appendLast(s2);
        gs.appendLast(s3);
        gp.appendLast(p1);
        gp.appendLast(p2);
        gp.appendLast(p3);
        gp.appendLast(p4);
        s = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        };
        sh = new ComparatorStringHash();
        pd = new Comparator<Pizza>() {
            @Override
            public int compare(Pizza o1, Pizza o2) {
                return o1.compareTo(o2);
            }
        };
    }

    @Test
    void test() {
        assertEquals(s3,MyUtil.max(gs), "max1 Fehler");
        assertEquals(p4,MyUtil.max(gp), "max1 Fehler");
        assertEquals(s3, MyUtil.max(gs,s));
        assertEquals(s3, MyUtil.max(gs,sh));
        assertEquals(p4,MyUtil.max(gp,pd));
    }
}
