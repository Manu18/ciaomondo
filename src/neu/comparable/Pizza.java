package neu.comparable;

import neu.junit.Gericht;

public class Pizza extends Gericht implements Comparable<Pizza> {
    private int diameter = 0;

    public Pizza() {
        super("2 - Haupspeise",15);
    }
    public Pizza(int p, int d) {
        super("2 - Haupspeise", p);
        setDiameter(d);
    }
    public int getDiameter() {
        return diameter;
    }
    public void setDiameter(int d) {
        diameter = d;
    }
    public String toString() {
        String s = super.toString() + " mit einem Durchmesser von " + getDiameter();
        return s;
    }
    public int compareTo(Pizza t) {
        int c = this.getDiameter() - t.getDiameter();
        return c;
    }
}
