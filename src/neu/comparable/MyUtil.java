package neu.comparable;

import neu.generics.GroupIFG;
import java.util.Comparator;

public class MyUtil {
    public static <T extends Comparable<T>> T max(GroupIFG<T> gi) {
        if (gi != null) {
            T max = gi.get(1);
            for (int i = 2; i <= gi.size(); i++) {
                max = (max.compareTo(gi.get(i)) > 0) ? max : gi.get(i);
            }
            return max;
        } else {
            throw new NullPointerException();
        }
    }

    public static <T> T max(GroupIFG<T> a, Comparator<? super T> comp) {
        if (a != null && comp != null) {
            int k = 1;
            for (int i = 2; i <= a.size(); i++) {
                if (comp.compare(a.get(i), a.get(k)) > 0) {
                    k = i;
                }
            }
            return a.get(k);
        } else {
            throw new NullPointerException();
        }
    }

}
