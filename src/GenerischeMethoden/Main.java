package GenerischeMethoden;

public class Main {
    public static void main(String[] args) {
        Pizza p1 = new Pizza(5, 20);
        Pizza p2 = new Pizza(7, 20);
        Salat s1 = new Salat(4, 200);
        String str = "Hello World";
        int z = 42;
        // A2
        System.out.println(Gericht.getMoreExpensive(p1, p2).getClass());
        System.out.println(Gericht.getMoreExpensive(p1, s1).getClass());
        System.out.println(Gericht.getMoreExpensive(s1, p1).getClass());
        System.out.println(Gericht.getMoreExpensive(str, z).getClass());
        System.out.println(Gericht.getMoreExpensive(z, str).getClass());
    }
}
