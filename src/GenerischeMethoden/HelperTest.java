package GenerischeMethoden;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class HelperTest {
	Object[] feld = {2, "Hallo", "Hallo", new String("Hallo"), new Pizza(), new Salat(), new Pizza()};

	@Test
	void test() {
		for(Object o1:feld) {
			for(Object o2:feld) {
				if(o1.toString().length()>o2.toString().length())
					assertEquals(o1, Helper.stringLaenge(o1, o2), "Problem mit stringLaenge(..)");
				else
					assertEquals(o2, Helper.stringLaenge(o1, o2), "Problem mit stringLange(...)");
			}
		}
	}

	@Test
	void getMoreExpensiveTest() {
		Pizza p1 = new Pizza(5, 20);
		Pizza p2 = new Pizza(7, 20);
		Salat s1 = new Salat(4, 200);
		String str = "Hello World";
		int z = 42;
		assertEquals(p1.getClass(), Gericht.getMoreExpensive(p1,p2).getClass(), "Falscher Rückgabewert");
		assertEquals(p1.getClass(), Gericht.getMoreExpensive(p1,s1).getClass(), "Falscher Rückgabewert");
		assertEquals(s1.getClass(), Gericht.getMoreExpensive(s1,p1).getClass(), "Falscher Rückgabewert");
		assertEquals(str.getClass(), Gericht.getMoreExpensive(str,z).getClass(), "Falscher Rückgabewert");
		assertSame(z, Gericht.getMoreExpensive(z,str), "Falscher Rückgabewert");
		assertSame(p1, Gericht.getMoreExpensive(p1,str), "Falscher Rückgabewert");
	}

}
