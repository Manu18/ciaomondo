package GenerischeMethoden;

public class Gericht {
    private String course = "";
    private int price = 0;

    public Gericht() {
        this.course = "Bestseller";
        this.price = 18;
    }

    public Gericht(String s, int i) {
        this.course = s;
        this.price = i;
    }

    public String getCourse() {
        return course;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return "Gericht";
    }

    // A1
    public static <T> T getMoreExpensive(T g1, T g2) {
        return g1;
    }
    // mit ueberpruefung:
    public static <T> T getMoreExpensive2(T g1, T g2) {
        if (g1.getClass().equals(g2.getClass())) {
            // Gleiches objekt
            if (g1 instanceof Gericht) {
                // g1, g2 sind gerichte
                if (((Gericht) g1).getPrice() > ((Gericht) g2).getPrice()) {
                    return g1;
                } else {
                    return g2;
                }
            }
        }
        // Fehler
        return null;
    }
}
