package GenerischeMethoden;

public class Salat extends Gericht {
    private int weight = 0;

    public Salat() {
        super("Salat",5);
    }

    public Salat(int p, int w) {
        super("Salat",p);
        this.weight = w;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int w) {
        this.weight = w;
    }

    public String toString() {
        return "Salat mit Gewicht von " + getWeight();
    }
}
