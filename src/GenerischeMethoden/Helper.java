package GenerischeMethoden;

public class Helper {
    // A3
    public static <T> T stringLaenge(T s1, T s2) {
        StringComparator sc = new StringComparator();
        return sc.compare(s1.toString(), s2.toString()) > 0 ? s1 : s2;
    }

    public static <T> T stringLaenge2(T s1, T s2) {
        if (s1.toString().length() > s2.toString().length()) {
            return s1;
        } else {
            return s2;
        }
    }
}
