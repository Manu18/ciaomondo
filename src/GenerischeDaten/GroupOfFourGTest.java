package GenerischeDaten;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GroupOfFourGTest {
    GroupOfFourG<Pizza> pizzaGroup;
    Pizza p1;
    Pizza p2;
    Pizza p3;
    Pizza p4;
    Pizza[] pizzas = new Pizza[4];

    @BeforeEach
    void setup() throws TableSpaceOutOfBoundsException {
        this.p1 = new Pizza(1,20);
        this.p2 = new Pizza(2,20);
        this.p3 = new Pizza(3,20);
        this.p4 = new Pizza(4,20);
        pizzas[0] = p1;
        pizzas[1] = p2;
        pizzas[2] = p3;
        pizzas[3] = p4;
        this.pizzaGroup = new GroupOfFourG<Pizza>();
    }

    @Test
    void appendLastTest() throws TableSpaceOutOfBoundsException {
        pizzaGroup.appendLast(p1);
        assertEquals(p1, pizzaGroup.get(1), "Objekte werden nicht an die richtige Position gespeichert");
        pizzaGroup.appendLast(p2);
        assertEquals(p2, pizzaGroup.get(2), "Objekte werden nicht an die richtige Position gespeichert");
        pizzaGroup.appendLast(p3);
        assertEquals(p3, pizzaGroup.get(3), "Objekte werden nicht an die richtige Position gespeichert");
        pizzaGroup.appendLast(p4);
        assertEquals(p4, pizzaGroup.get(4), "Objekte werden nicht an die richtige Position gespeichert");

        Pizza p5 = new Pizza(5,20);
        assertThrows(TableSpaceOutOfBoundsException.class, () -> pizzaGroup.appendLast(p5), "Fehler zu Kein Platz mehr am Tisch wird nicht geworfen");
    }

    @Test
    void sizeTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            assertEquals(i, pizzaGroup.size(), "Falsche Anzahl an belegten Plaetzen");
            pizzaGroup.appendLast(p1);
        }
    }

    @Test
    void removeLastTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            pizzaGroup.appendLast(pizzas[i]);
        }
        for (int i = 4; i > 0; i--) {
            assertEquals(i, pizzaGroup.removeLast().getPrice(), "Objekte werden nicht richtig zurueckgegeben beim Loeschen");
        }
    }

    @Test
    void getTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            pizzaGroup.appendLast(pizzas[i]);
        }
        for (int i = 4; i > 0; i--) {
            assertEquals(i, pizzaGroup.removeLast().getPrice(), "Objekte werden nicht richtig mit get zurueckgegeben");
        }
    }

    @Test
    void swapTest() throws TableSpaceOutOfBoundsException {
        pizzaGroup.appendLast(p1);
        pizzaGroup.appendLast(p2);
        pizzaGroup.swap(1,2);
        assertEquals(p2.getPrice(), pizzaGroup.get(1).getPrice(), "Swap funktioniert nicht richtig");
        assertEquals(p1.getPrice(), pizzaGroup.get(2).getPrice(), "Swap funktioniert nicht richtig");
    }

    // U3
    /*
    Ueberlegen Sie sich zwei weitere moegliche (logische) Fehler und testen Sie Ihre Implementierung darauf.
    Veraendern Sie dabei Ihre Implementierungen der GroupOfFour-Klasse so, dass diese Fehler tatsaechlich auftreten.
     */

    // swap mit leerem Objekt
    @Test
    void swapEmptyTest() throws TableSpaceOutOfBoundsException {
        pizzaGroup.appendLast(p1);
        assertThrows(NullPointerException.class, () -> pizzaGroup.swap(1,3), "Swap Fehler mit leerem Element Fehler nicht erzeugt");
    }

    // get ausserhalb vom Index
    @Test
    void getIntdexTest() {
        assertThrows(IndexOutOfBoundsException.class, () -> pizzaGroup.get(5), "Get Index Fehler wurde nicht erzeugt");
        assertThrows(IndexOutOfBoundsException.class, () -> pizzaGroup.get(0), "Get Index Fehler wurde nicht erzeugt");
    }

    // removeLast obwohl schon leer
    @Test
    void removeLastEmptyTest() {
        assertThrows(NullPointerException.class, () -> pizzaGroup.removeLast(), "Remove Last obwohl leer Fehler wurde nicht erzeugt");
    }

    // weitere Konstruktoren test, anzahl der eingefuegten und gleichheit wird ueberprueft
    @Test
    void constructorTests() throws TableSpaceOutOfBoundsException {
        GroupOfFourG<Pizza> testg1 = new GroupOfFourG<>(p1);
        assertEquals(1, testg1.size(), "Konstruktor funktioniert nicht wie erwartet");
        assertEquals(p1.getPrice(), testg1.removeLast().getPrice(), "Konstruktor funktioniert nicht wie erwartet");


        GroupOfFourG<Pizza> testg2 = new GroupOfFourG<>(p1,p2);
        assertEquals(2, testg2.size(), "Konstruktor funktioniert nicht wie erwartet");
        assertEquals(p2.getPrice(),testg2.removeLast().getPrice(), "Konstruktor funktioniert nicht wie erwartet");


        GroupOfFourG<Pizza> testg3 = new GroupOfFourG<>(p1,p2,p3);
        assertEquals(3, testg3.size(), "Konstruktor funktioniert nicht wie erwartet");
        assertEquals(p3.getPrice(),testg3.removeLast().getPrice(), "Konstruktor funktioniert nicht wie erwartet");

        GroupOfFourG<Pizza> testg4 = new GroupOfFourG<>(p1,p2,p3,p4);
        assertEquals(4, testg4.size(), "Konstruktor funktioniert nicht wie erwartet");
        assertEquals(p4.getPrice(),testg4.removeLast().getPrice(), "Konstruktor funktioniert nicht wie erwartet");
    }

    //GroupOfFourG<Pizza>, Einfügen von Salat-Objekten (dies sollte nicht möglich sein!).
    @Test
    void datatypeTest() throws TableSpaceOutOfBoundsException {
        Salat s1 = new Salat(1,12);
        Salat s2 = new Salat(2,12);
        Salat s3 = new Salat(3,12);
        GroupOfFourG<Pizza> pizzaSalatTest = new GroupOfFourG<>(p1);
        //assertThrows(IllegalArgumentException.class, () -> pizzaSalatTest.appendLast(s1), "Keine Fehler ausgegeben?");
        // pizzaSalatTest.appendLast(s1) nicht möglich

        GroupOfFourG<Gericht> salatTest = new GroupOfFourG<>(s1,s2);
        salatTest.appendLast(s3);
        assertEquals(s3.getPrice(), salatTest.removeLast().getPrice(), "Keine Salate in Gerichte?");
        // Keine Casts benoetigt

        GroupOfFourG<Gericht> gerichteTest = new GroupOfFourG<>(p1,s1);
        gerichteTest.appendLast(p2);
        gerichteTest.appendLast(s2);
        assertEquals(s2.getPrice(), gerichteTest.removeLast().getPrice(), "Verschiedene Datentypenproblem");
        // Keine Casts benoetigt

        // weitere faelle:
        // swap von verschiedenen datentypen
        GroupOfFourG<Gericht> swapTest = new GroupOfFourG<>(p1, s1, p2, s2);
        swapTest.swap(1,2);
        assertEquals(swapTest.get(1), s1, "Swap funktioniert nicht bei verschiedenen Datenypen");
        assertEquals(swapTest.get(2), p1, "Swap funktioniert nicht bei verschiedenen Datenypen");

        // toString mit verschiedenen Datentypen
        swapTest.toString();

        //siehe constructorTest()

    }
}
