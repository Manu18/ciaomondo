package GenerischeDaten;

import Comparator_Iterator.Comparator.ComparatorStringHash;

public class Main {
    public static void main(String[] args) throws TableSpaceOutOfBoundsException {
        Pizza PizzaFeld[] = {new Pizza(), new Pizza(), new Pizza()};
        Salat SalatFeld[] = {new Salat(), new Salat(), new Salat()};
        Gericht GerichtFeld[] = {new Pizza(), new Salat(), new Pizza(), new Salat(), new Pizza()};

        GroupOfFourG<Pizza> PizzaGr = new GroupOfFourG<>(PizzaFeld);
        //GroupOfFourG<Gericht> GerichtGr = new GroupOfFourG<>(GerichtFeld);
        GroupOfFourG<Gericht> GerichtGr = new GroupOfFourG<>(SalatFeld);
        GerichtGr.appendLast(new Pizza());
        // Es ist kein Problem, da Pizza und Salat vom Typ Gericht sind

        ComparatorStringHash testC = new ComparatorStringHash();
        String a = "HALLOMeineLieben";
        String b = "HalloWelt";
        System.out.println(a.hashCode());
        // -2124471979
        System.out.println(b.hashCode());
        // 524476716
        System.out.println(testC.compare(a,b));
        // Problem overflow von int, erwartet waere ein Ergebnis von compare(a,b) < 0




    }
}
