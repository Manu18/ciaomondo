package GenerischeDaten;

public class Pizza extends Gericht {
    private int diameter = 0;

    public Pizza() {
        super("Pizza",10);
    }

    public Pizza(int p, int d) {
        super("Pizza",p);
        this.diameter = d;
    }

    public void setDiameter(int d) {
        this.diameter = d;
    }

    public int getDiameter() {
        return this.diameter;
    }

    public String toString() {
        return "Pizza mit Durchmesser von " + getDiameter();
    }
}
