package GenerischeDaten;

public interface GroupIFG<T> {
    int size();
    boolean isEmpty();
    void appendLast(T g) throws TableSpaceOutOfBoundsException;
    T removeLast();
    T get(int p);
    void swap(int p1, int p2);
}
