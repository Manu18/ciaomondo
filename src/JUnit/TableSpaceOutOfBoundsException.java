package JUnit;

public class TableSpaceOutOfBoundsException extends Exception {
    long serialVersionUID = 1L;

    public TableSpaceOutOfBoundsException() {
        super("Fehler aufgetreten");
    }

    public TableSpaceOutOfBoundsException(String m) {
        super(m);
    }

}
