package JUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GroupOfFourTest {
    GroupOfFour group;
    Gericht testgericht1;
    Gericht testgericht2;
    Gericht testgericht3;
    Gericht testgericht4;
    Gericht[] testgerichte = new Gericht[4];

    @BeforeEach
    void setup() {
        this.group = new GroupOfFour();
        this.testgericht1 = new Pizza(1,20);
        this.testgericht2 = new Pizza(2,20);
        this.testgericht3 = new Pizza(3,20);
        this.testgericht4 = new Pizza(4,20);
        testgerichte[0] = testgericht1;
        testgerichte[1] = testgericht2;
        testgerichte[2] = testgericht3;
        testgerichte[3] = testgericht4;
    }

    @Test
    void appendLastTest() throws TableSpaceOutOfBoundsException {
        group.appendLast(testgericht1);
        assertEquals(testgericht1, group.get(1), "Gerichte werden nicht an die richtige Position gespeichert");
        group.appendLast(testgericht2);
        assertEquals(testgericht2, group.get(2), "Gerichte werden nicht an die richtige Position gespeichert");
        group.appendLast(testgericht3);
        assertEquals(testgericht3 ,group.get(3), "Gerichte werden nicht an die richtige Position gespeichert");
        group.appendLast(testgericht4);
        assertEquals(testgericht4, group.get(4), "Gerichte werden nicht an die richtige Position gespeichert");

        Gericht testgericht5 = new Pizza(5, 20);
        assertThrows(TableSpaceOutOfBoundsException.class, () -> group.appendLast(testgericht5), "Fehler zu Kein Platz mehr am Tisch wird nicht geworfen");
    }

    @Test
    void sizeTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            assertEquals(i, group.size(), "Falsche Anzahl an belegten Plaetzen");
            group.appendLast(testgericht1);
        }
    }

    @Test
    void removeLastTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            group.appendLast(testgerichte[i]);
        }
        for (int i = 4; i > 0; i--) {
            assertEquals(i, group.removeLast().getPrice(), "Gerichte werden nicht richtig zurueckgegeben beim Loeschen");
        }
    }

    @Test
    void getTest() throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4; i++) {
            group.appendLast(testgerichte[i]);
        }
        for (int i = 4; i > 0; i--) {
            assertEquals(i, group.removeLast().getPrice(), "Gerichte werden nicht richtig mit get zurueckgegeben");
        }
    }

    @Test
    void swapTest() throws TableSpaceOutOfBoundsException {
        group.appendLast(testgericht1);
        group.appendLast(testgericht2);
        group.swap(1,2);
        assertEquals(testgericht2.getPrice(),group.get(1).getPrice(), "Swap funktioniert nicht richtig");
        assertEquals(testgericht1.getPrice(),group.get(2).getPrice(), "Swap funktioniert nicht richtig");
    }

    // U3
    /*
    Ueberlegen Sie sich zwei weitere moegliche (logische) Fehler und testen Sie Ihre Implementierung darauf.
    Veraendern Sie dabei Ihre Implementierungen der GroupOfFour-Klasse so, dass diese Fehler tatsaechlich auftreten.
     */

    // swap mit leerem Objekt
    @Test
    void swapEmptyTest() throws TableSpaceOutOfBoundsException {
        group.appendLast(testgericht1);
        assertThrows(NullPointerException.class, () -> group.swap(1,3), "Swap Fehler mit leerem Element Fehler nicht erzeugt");
    }

    // get ausserhalb vom Index
    @Test
    void getIntdexTest() {
        assertThrows(IndexOutOfBoundsException.class, () -> group.get(5), "Get Index Fehler wurde nicht erzeugt");
        assertThrows(IndexOutOfBoundsException.class, () -> group.get(0), "Get Index Fehler wurde nicht erzeugt");
    }

    // removeLast obwohl schon leer
    @Test
    void removeLastEmptyTest() {
        assertThrows(NullPointerException.class, () -> group.removeLast(), "Remove Last obwohl leer Fehler wurde nicht erzeugt");
    }

    // U4 Tausch der Testklasse mit anderen Studenten
    // U5 Speicherung der Sammlung von Objekten mit Variablen des Typs Object
    /*
    Vorteil:
        - toString, clone, equals Methode vorhanden
        - Es können alle Datentypen / Variablen gespeichert werden, da alle vom Typ Object abgeleitet sind. => Flexibler einsetzbar
    Nachteil:
        - Eigenschaften werden vedeckt, durch upcast
        - instanceof bennoetigt, bei mehreren Variablentypen => Fehlerquelle
     */
}
