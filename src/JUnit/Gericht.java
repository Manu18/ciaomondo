package JUnit;

public abstract class Gericht {
    private String course = "";
    private int price = 0;

    public Gericht() {
        this.course = "Bestseller";
        this.price = 18;
    }

    public Gericht(String s, int i) {
        this.course = s;
        this.price = i;
    }

    public String getCourse() {
        return course;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return "Gericht";
    }
}
