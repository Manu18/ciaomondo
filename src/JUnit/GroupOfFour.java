package JUnit;

public class GroupOfFour implements GroupIF {
    private int size = 0;
    protected Gericht ersterPlatz;
    protected Gericht zweiterPlatz;
    private Gericht dritterPlatz;
    private Gericht vierterPlatz;

    public GroupOfFour() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void appendLast(Gericht g) throws TableSpaceOutOfBoundsException {
        switch (size()) {
            case 0:
                ersterPlatz = g;
                size++;
                break;
            case 1:
                zweiterPlatz = g;
                size++;
                break;
            case 2:
                dritterPlatz = g;
                size++;
                break;
            case 3:
                vierterPlatz = g;
                size++;
                break;
            case 4:
                // voll
                throw new TableSpaceOutOfBoundsException("Kein Platz mehr am Tisch");
        }
    }

    @Override
    public Gericht removeLast() {
        Gericht retG = null;
        switch (size()) {
            case 0:
                 throw new NullPointerException("Speicher ist leer, keine Gerichte gespeichert");
            case 1:
                size--;
                retG = ersterPlatz;
                ersterPlatz = null;
                break;
            case 2:
                size--;
                retG = zweiterPlatz;
                zweiterPlatz = null;
                break;
            case 3:
                size--;
                retG = dritterPlatz;
                dritterPlatz = null;
                break;
            case 4:
                size--;
                retG = vierterPlatz;
                vierterPlatz = null;
                break;
        }
        return retG;
    }

    @Override
    public Gericht get(int p) {
        switch (p) {
            case 0:
                throw new IndexOutOfBoundsException("Kein Platz mit dem Index vorhanden");
            case 1:
                return ersterPlatz;
            case 2:
                return zweiterPlatz;
            case 3:
                return dritterPlatz;
            case 4:
                return vierterPlatz;
        }
        throw new IndexOutOfBoundsException("Kein Platz mit dem Index vorhanden");
    }

    @Override
    public void swap(int p1, int p2) {
        Gericht c1 = this.get(p1);
        Gericht c2 = this.get(p2);

        if (c1 == null | c2 == null) {
            throw new NullPointerException("Swap mit leerem Objekt nicht möglich");
        }

        switch (p1) {
            case 1:
                this.ersterPlatz = c2;
            case 2:
                this.zweiterPlatz = c2;
            case 3:
                this.dritterPlatz = c2;
            case 4:
                this.vierterPlatz= c2;
        }
        switch (p2) {
            case 1:
                this.ersterPlatz = c1;
            case 2:
                this.zweiterPlatz = c1;
            case 3:
                this.dritterPlatz = c1;
            case 4:
                this.vierterPlatz = c1;
        }
    }

    public void clear() {
        size = 0;
        ersterPlatz = null;
        zweiterPlatz = null;
        dritterPlatz = null;
        vierterPlatz = null;
    }

    public String toString() {
        String s = "Tisch mit " + size() + " Personen: \n";
        if (size() > 0)
            s += "Platz 1 hat bestellt: " + ersterPlatz.toString();
        if (size() > 1)
            s += "Platz 2 hat bestellt: " + zweiterPlatz.toString();
        if (size() > 2)
            s += "Platz 3 hat bestellt: " + dritterPlatz.toString();
        if (size() > 3)
            s += "Platz 4 hat bestellt: " + vierterPlatz.toString();
        return s;
    }
}
