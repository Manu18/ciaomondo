package JUnit;

import Einführung.Fifo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FiFoTest {
    Fifo f;
    static Integer ai[] = {1,2,3,4,5};

    @BeforeEach
    void setup() {
        f = new Fifo(5);
    }

    @AfterEach
    void teardwon() {
        f = null;
    }

    @Test
    void testPushPop() {
        for (Integer integer : ai) {
            f.push(integer);
            assertEquals(integer,f.pop(), "Fehler bei pop Methode");

        }
    }

    @Test
    void testSizeCapacity() {
        int s = 0;
        for(Integer i : ai) {
            assertEquals(f.size(), s, "Groesse stimmt nicht");
            assertEquals(f.capacity(), f.array.length, "Kapazitaet stimmt nicht");
            f.push(i);
            s++;
        }
    }

    @Test
    void testPositon() {
        int p = 0;
        for (Integer i : ai) {
            f.push(i);
            assertEquals(f.array[p],ai[p],"Positon stimmt nicht");
            p++;
        }
    }

    @Test
    void testPopException() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> f.pop(), "Keine ArrayOutOfBoundsException");
    }
}
