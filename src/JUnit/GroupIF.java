package JUnit;

public interface GroupIF {
    int size();
    boolean isEmpty();
    void appendLast(Gericht g) throws TableSpaceOutOfBoundsException;
    Gericht removeLast();
    Gericht get(int p);
    void swap(int p1, int p2);
}
