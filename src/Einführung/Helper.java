package Einführung;

public class Helper {
    static void move(IntegerBuffer quelle, IntegerBuffer ziel) throws IllegalArgumentException {
        int counter = quelle.size();
        for (int i = 0; i < counter; i++) {
            if (ziel.size() == ziel.capacity()) {
                throw new IllegalArgumentException("Nicht genug Platz im Ziel");
            }
            ziel.push(quelle.pop());
        }
    }
}
