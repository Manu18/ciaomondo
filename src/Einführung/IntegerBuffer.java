package Einführung;

public interface IntegerBuffer {
    public void push(Integer i); // push i into buffer
    public Integer pop(); // get Integer from buffer
    public int size(); // number of Integers in the buffer
    public int capacity(); // maximum size of the buffer 6
}
