package Einführung;

public abstract class AbstractIntegerBuffer implements IntegerBuffer {
    int size = 0;
    public Integer[] array;
    int capacity;

    public void push(Integer i) throws ArrayIndexOutOfBoundsException, IllegalArgumentException {
        if (size == capacity) {
            throw new ArrayIndexOutOfBoundsException("Fehler, Speicher bereits voll");
        } else {
            array[size++] = i;
        }
    }
    public int maxSize() {
        return array.length;
    }
    public int size() {
        return size;
    }
    public int capacity() {
        return capacity;
    }
}
