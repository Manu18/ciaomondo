package Einführung;

import java.util.Random;

public class Fifo extends AbstractIntegerBuffer {

    public Fifo() {
        Random r = new Random();
        int s;
        do {
            s = r.nextInt(100);
        } while (s <= 3);
        array = new Integer[s];
        size = 0;
        capacity = s;
    }

    public Fifo(int s) {
        array = new Integer[s];
        size = 0;
        capacity = s;
    }

    public Integer pop() throws ArrayIndexOutOfBoundsException {
        Integer c = array[0];
        if (size > 0) {
            for (int i = 0; i < size - 1; i++) {
                array[i] = array[i+1];
            }
            size--;
        } else {
            throw new ArrayIndexOutOfBoundsException("Fehler, Speicher bereits leer");
        }
        return c;
    }



}