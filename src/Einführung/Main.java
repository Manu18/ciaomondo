package Einführung;

public class Main {

    public static void main(String[] args) {
        IntegerBuffer quelle = new Fifo();
        IntegerBuffer ziel = new Lifo();
        // Fehler werfen eigentlich nicht notwendig, da die von selbst geworfen werden.

        try {
            //for (int i = 0; i < quelle.capacity(); i++) {
              //  quelle.push(i);
            //}
            quelle.push(0);
            quelle.push(1);
            quelle.push(2);

            //System.out.println(quelle.capacity());
            //System.out.println(quelle.size());


            while (quelle.size() > 0) {
                System.out.println(quelle.pop());
            }

            quelle.push(10);
            quelle.push(11);

            System.out.println(quelle.pop());
            System.out.println(quelle.pop());


            for (int i = 100; i < ziel.capacity() + 100; i++) {
                ziel.push(i);
            }
            //System.out.println(ziel.capacity());
            //System.out.println(ziel.size());
            while (ziel.size() > 0) {
                System.out.println(ziel.pop());
            }
             /*
            Helper.move(quelle, ziel);
            while (ziel.size() > 0) {
                System.out.println(ziel.pop());
            }
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
