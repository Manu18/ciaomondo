package Einführung;

interface LambdaInterface {
    void lamdaM(int i);
}
public class Lambda {
    static void doit(LambdaInterface i) {
        System.out.print("Doit: ");
        i.lamdaM(1);;
    }
    public static void main(String[] args) {
        LambdaInterface li = new LambdaInterface() {
            @Override
            public void lamdaM(int i) {
                System.out.println(i + " normal");
            }
        };
        doit(li);
        doit((a -> System.out.println(2 + " lamda") ));
    }
}
