package Einführung;

import java.nio.file.LinkOption;
import java.util.Random;

public class Lifo extends AbstractIntegerBuffer {

    public Lifo() {
        Random r = new Random();
        int s;
        do {
            s = r.nextInt(100);
        } while (s <= 3);
        array = new Integer[s];
        size = 0;
        capacity = s;
    }

    public Lifo(int s) {
        array = new Integer[s];
        size = 0;
        capacity = s;
    }

    public Integer pop() throws ArrayIndexOutOfBoundsException {
        if (size < 1) {
            throw new ArrayIndexOutOfBoundsException("Fehler, Speicher bereits leer");
        }
        return array[--size];
    }
}

