package DMLA_Test;

import java.util.*;

public class A2 {

    public static long n0(double eps){
        return (long)(Math.ceil(9.0 / (4.0 * (eps * eps))));
    }

    public static long n1(double eps){
        return (long)(Math.ceil(46 / (5 * eps)));
    }

    public static long n2(double M){
        // return (long)(Math.ceil(33.9705627484771 * M * M));
        //return (long)(Math.ceil((17*M*M) + (4 * Math.sqrt(6) * Math.sqrt(3*(M*M*M*M) - 4*(M*M)) - 12)));
        return (long)(Math.ceil((M*M) * (17 + 12 * Math.sqrt(2))));
    }


}