package Typableitung_Inferenz_Wildcard;/*
 * Was passiert, wenn Sie der GroupOfFourG<Gericht> ein
 * GroupOfFourG<>(SalatFeld) zuweisen? --- Da Salat ein Kind von Gerichte ist,
 * werden alle Salate zu Gerichten gecastet ----------- Können Sie in diese
 * Gericht-Sammlung von Objekten anschliessend eine Pizza einfuegen? --- Die
 * Gerichte-Sammlung ist auch dann eine Gerichte-Sammlung, wenn in ihr nur
 * Salate enthalten sind. Zu einer Gerichte-Sammlung kann man jederzeit eine
 * Pizza (das sie auch ein Gericht ist) hinzufuegen. Es ist also moeglich eine
 * Pizza dort einzufuegen.
 */

/**
 * Eine {@code GroupOfFour} hat vier Plaetze, die nacheinander aufgefuellt und
 * abgeraeumt oder an einem Platz ausgelesen werden koennen
 * 
 * @param <T> egal
 * @since LE01A1
 * @version LE04_5A1G
 */
public class GroupOfFour<T> implements GroupIF<T> {
	// Instanzvariablen

	/**
	 * Anzahl der belegten Speicherpluetze
	 */
	private int size;

	/**
	 * erster Platz fuer ein {@link T T}
	 */
	protected T ersterPlatz;

	/**
	 * zweiter Platz fuer ein {@link T T}
	 */
	protected T zweiterPlatz;

	/**
	 * dritter Platz fuer ein {@link T T}
	 */
	private T dritterPlatz;

	/**
	 * vierter Platz fuer ein {@link T T}
	 */
	private T vierterPlatz;

	// Konstruktoren

	/**
	 * Erzeugt eine leere {@code GroupOfFourG}
	 */
	public GroupOfFour() {
	}

	/**
	 * Die maximal vier ersten Elemente des {@code T-arrays} werden auf die laetze
	 * der {@code GroupOfFourG} gesetzt
	 * 
	 * @param array
	 */
	public GroupOfFour(T[] array) {
		switch (4 - array.length) {
			case 0:
				vierterPlatz = array[3];
				// weiter
			case 1:
				dritterPlatz = array[2];
				// weiter
			case 2:
				zweiterPlatz = array[1];
				// weiter
			case 3:
				ersterPlatz = array[0];
		}
		size = Integer.min(4, array.length);
	}

	// Instanzmethoden

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void appendLast(T g) throws TableSpaceOutOfBoundsException {
		switch (size) {
			case 0:
				ersterPlatz = g;
				break;
			case 1:
				zweiterPlatz = g;
				break;
			case 2:
				dritterPlatz = g;
				break;
			case 3:
				vierterPlatz = g;
				break;
			case 4:
				throw new TableSpaceOutOfBoundsException("Es sind bereits alle Plaetze belegt");
		}
		size++;
	}

	public T removeLast() throws TableSpaceOutOfBoundsException {
		T ret = null;
		switch (size) {
			case 4:
				ret = vierterPlatz;
				break;
			case 3:
				ret = dritterPlatz;
				break;
			case 2:
				ret = zweiterPlatz;
				break;
			case 1:
				ret = ersterPlatz;
				break;
			case 0:
				throw new TableSpaceOutOfBoundsException("Es ist kein Platz belegt");
		}
		size--;
		return ret;
	}

	public T get(int p) throws TableSpaceOutOfBoundsException {
		if (size < p) {
			p = 5;
		}
		switch (p) {
			case 4:
				return vierterPlatz;
			case 3:
				return dritterPlatz;
			case 2:
				return zweiterPlatz;
			case 1:
				return ersterPlatz;
			default:
				throw new TableSpaceOutOfBoundsException("Ungueltige oder nicht belegte Position");
		}
	}

	public void swarp(int p1, int p2) throws TableSpaceOutOfBoundsException {
		try {
			if (p1 < 1 || size < p1) {
				throw new TableSpaceOutOfBoundsException("p1 ist ungueltig oder nicht belegt");
			} else if (p2 < 1 || size < p2) {
				throw new TableSpaceOutOfBoundsException("p2 ist ungueltig oder nicht belegt");
			} else {
				@SuppressWarnings("unchecked")
				T[] array = (T[]) new Object[] { ersterPlatz, zweiterPlatz, dritterPlatz, vierterPlatz };
				T temp = array[p1 - 1];
				array[p1 - 1] = array[p2 - 1];
				array[p2 - 1] = temp;
				ersterPlatz = array[0];
				zweiterPlatz = array[1];
				dritterPlatz = array[2];
				vierterPlatz = array[3];
			}
		} catch (ClassCastException e) {
			System.out.println("Fehler in GroupOfFour.swarp: " + e.getMessage() + ", mit: ");
			e.printStackTrace();
		}
	}

	/**
	 * loescht alle Eintraege
	 */
	public void clear() {
		size = 0;
		ersterPlatz = null;
		zweiterPlatz = null;
		dritterPlatz = null;
		vierterPlatz = null;
	}

	@Override
	public String toString() {
		String ret = "Belegung:";
		switch (size) {
			case 0:
				ret = "Die GroupOfFourG ist leer, ";
				break;
			case 4:
				ret += "\nvierter Platz: " + vierterPlatz + ", ";
				// weiter
			case 3:
				ret += "\ndritter Platz: " + dritterPlatz + ", ";
				// weiter
			case 2:
				ret += "\nzweiter Platz: " + zweiterPlatz + ", ";
				// weiter
			case 1:
				ret += "\nerster Platz: " + ersterPlatz + ", ";
				// weiter
		}
		ret += "\b\b";
		return ret;
	}
}
