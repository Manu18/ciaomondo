package Typableitung_Inferenz_Wildcard;

import GenerischeMethoden.Gericht;

public class GroupOfFourGerichte extends GroupOfFour<Gericht> {

    /*
    Sie brauchen eine neue Methode, die Ihnen den Gesamtpreis aller Gerichte in einer GroupOfFour angibt.
    Speziﬁzieren und implementieren Sie dafür eine von GroupOfFour abgeleitete Klasse GroupOfFourGerichte mit der Methode totalPrice().
     Dabei stoßen Sie auf ein Problem: Ihre generischeKlassekannfürjedebeliebigeParameter-Klasseinstanziiertwerden,undderCompiler
     weiß nicht, ob diese Klassen alle die benötigte Funktion price implementieren. Daher müssen Sie dieMengedererlaubten
     ObjekteinderSammlungeinschränken:Siebraucheneine GroupOfFour, die lediglich Gerichte aufnimmt. Es soll möglich sein,
     sie als GroupOfFourGerichte<Pizza> oder GroupOfFourGerichte<Eis> zu instanziieren, nicht aber als GroupOfFourGerichte<Bestellung>
     oder GroupOfFourGerichte<Apfelbaum>. Implementieren Sie die Klasse GroupOfFourGerichte<> einschließlich geeigneter Exceptions
     und Unittests.

     */

    int totalPrice() {
        int ret = 0;
        switch (4 - size()) {
            case 0:
                ret += get(4).getPrice();
                // weiter
            case 1:
                ret += get(3).getPrice();
                // weiter
            case 2:
                ret += get(2).getPrice();
                // weiter
            case 3:
                ret += get(1).getPrice();
        }
        return ret;
    }

    public static <T extends Gericht> T getMoreExpensive(T a, T b) {
        return (a.getPrice()>b.getPrice()) ? a : b;
    }

    public static <T> T getMoreExpensive2(T g1, T g2) {
        if (g1.getClass().equals(g2.getClass())) {
            // Gleiches objekt
            if (g1 instanceof Gericht) {
                // g1, g2 sind gerichte
                if (((Gericht) g1).getPrice() > ((Gericht) g2).getPrice()) {
                    return g1;
                } else {
                    return g2;
                }
            }
        }
        // Fehler
        return null;
    }

}
