package Typableitung_Inferenz_Wildcard;

/**
 * Die {@code TableSpaceOutOfBoundsException} ist eine Spezialisierung der
 * Klasse {@link RuntimeException RuntimeException} ohne zusaetzliche Funktionalitaet. Wir
 * verwenden lediglich diesen Bezeichner, um im Programmcode klarzustellen,
 * warum die Exception geworfen wird.
 * 
 * 
 * @since LE01A1
 * @version LE02A3C
 */
public class TableSpaceOutOfBoundsException extends RuntimeException {

    // Klassenvariable

    private static final long serialVersionUID = 1L;

    // Konstruktoren

    public TableSpaceOutOfBoundsException() {
        super("Ungueltiger Index");
    }

    public TableSpaceOutOfBoundsException(String message) {
        super(message);
    }
}
