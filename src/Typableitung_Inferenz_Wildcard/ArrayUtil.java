package Typableitung_Inferenz_Wildcard;

/**
 * Kopiert aus LE04_5A5G
 */
public class ArrayUtil<T> {
    /**
     * Sinnvoll ergaenzt
     * 
     * @param <T>
     * @param v1
     * @param v2
     * @return
     */
    public static <T> T[] kuerzer(T[] a1, T[] a2) {
        return a1.length < a2.length ? a1 : a2;
    }
}