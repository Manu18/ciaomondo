package Typableitung_Inferenz_Wildcard;

/**
 * Ein {@code Gericht} kann einen {@code Preis} und eine {@code Bezeichung}
 * haben
 * 
 * @since LE01A1
 * @version LE03A1
 */
public abstract class Gericht {

    // Instanzvariablen

    /**
     * die Bezeichnung
     */
    private String course;

    /**
     * der Preis
     */
    private int price;

    // Konstruktoren

    /**
     * neues undefiniertes {@code Gericht}
     */
    public Gericht() {
        course = "";
    }

    /**
     * neues spezielles {@code Gericht}
     * 
     * @param s der Bezeichnung
     * @param i der Preis
     */
    public Gericht(String s, int i) {
        course = s;
        price = i;
    }

    // Instanzmethoden

    /**
     * @return die Bezeichnung
     */
    public String getCourse() {
        return course;
    }

    /**
     * @return der Preis
     */
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return course + "    " + price;
    }

    // Klassenmethoden

    /**
     * Gibt das teuerer der beiden uebergebenden Gerichte zurueck.
     * 
     * @param <T> {@code Gericht}typ
     * @param a   ein {@code Gericht}
     * @param b   ein {@code Gericht} vom gleichen Typ wie {@code a}
     * @return das teuere der beiden {@code Gerichte}
     * @since LE03A1
     * @version LE04_5A2G
     */
    public static <T extends Gericht> T getMoreExpensive(T a, T b) {
        return a.getPrice() > b.getPrice() ? a : b;
    }
}