package Typableitung_Inferenz_Wildcard;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;

/**
 * Testet {@link GroupOfFourGerichte GroupOfFourGerichte}
 * 
 * @since LE04_5A1G
 * @version LE04_5A1G
 */
class GroupOfFourGerichteTest {

	Gericht[] gerichte = { new Pizza(5, 5), new Pizza(6, 6), new Pizza(7, 7), new Pizza(8, 8) };

	/**
	 * Testet, ob {@code totalPrice()} das richtige Ergebnis fuer 0-4 Gerichte in
	 * der {@code GroupOfFourGerichte} liefert.
	 * 
	 * @param ri entsteht durch Aufruf
	 */
	@RepeatedTest(5)
	void totalPriceTest(RepetitionInfo ri) {
		GroupOfFourGerichte gofg = new GroupOfFourGerichte();
		int soll = 0;
		for (int i = 0; i < ri.getCurrentRepetition() - 1; i++) {
			//gofg.appendLast(gerichte[i]);
		}
		switch (ri.getCurrentRepetition()) {
			case 2:
				soll = 5;
				break;
			case 3:
				soll = 11;
				break;
			case 4:
				soll = 18;
				break;
			case 5:
				soll = 26;
		}
		assertEquals(soll, gofg.totalPrice(),
				"totalPriceTest ist Falsch bei " + ri.getCurrentRepetition() + " Gerichten.");
	}
}
