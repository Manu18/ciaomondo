package Typableitung_Inferenz_Wildcard;

/**
 * Ein {@code Pizza} ist ein spezielles {@link Gericht Gericht}, das ein
 * {@code Durchmesser} hat
 * 
 * @since LE01A1
 * @version LE01A1
 */
public class Pizza extends Gericht {

	// Instanzvariable

	/**
	 * das Durchmesser
	 */
	private int diameter;

	// Konstruktoren

	/**
	 * ein neuer unspezifischer {@code Pizza}
	 */
	public Pizza() {
		super("Pizza", 0);
	}

	/**
	 * ein neuer spezifischer {@code Pizza}
	 * 
	 * @param w das Durchmesser der Preis
	 * @param i der Preis
	 */
	public Pizza(int p, int d) {
		super("Pizza", p);
		diameter = d;
	}

	// Instanzmethoden

	/**
	 * @return das Durchmesser
	 */
	public int getDiameter() {
		return diameter;
	}

	/**
	 * @param diameter das neue Durchmesser
	 */
	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	@Override
	public String toString() {
		return "Pizza (" + diameter + ")    " + getPrice();
	}
}
