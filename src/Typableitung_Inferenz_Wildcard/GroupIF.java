package Typableitung_Inferenz_Wildcard;

/**
 * @since LE01A1
 * @version LE02A3G
 */
public interface GroupIF<T> {

	// Instanzmethoden

	/**
	 * @return gibt die Anzahl der belegten Speicherplaetze an
	 */
	int size();

	/**
	 * @return besagt, ob alle Speicherplaetze leer sind oder nicht,
	 */
	boolean isEmpty();

	/**
	 * @param g fuegt das uebergebene Element am Ende ein
	 * @throws TableSpaceOutOfBoundsException wenn bereits alle Plaetze belegt sind
	 */
	void appendLast(T g) throws TableSpaceOutOfBoundsException;

	/**
	 * entfernt das letzte {@link T T} und gibt dieses wieder
	 * 
	 * @return letztes {@code T}
	 * @throws TableSpaceOutOfBoundsException wenn kein Platz belegt ist
	 */
	T removeLast() throws TableSpaceOutOfBoundsException;

	/**
	 * liefert das Objekt an der uebergebenen Position
	 * 
	 * @param p eine Position
	 * @return {@link T T} an der Position {@code p}
	 * @throws TableSpaceOutOfBoundsException wenn {@code p} ungueltig ist
	 */
	T get(int p) throws TableSpaceOutOfBoundsException;

	/**
	 * vertauscht die Objekte an den angegebenen Positionen
	 * 
	 * @param p1 eine Position
	 * @param p2 eine andere Position
	 * @throws TableSpaceOutOfBoundsException wenn {@code p1} oder {@code p2}
	 *                                        unueltig sind
	 */
	void swarp(int p1, int p2) throws TableSpaceOutOfBoundsException;
}
