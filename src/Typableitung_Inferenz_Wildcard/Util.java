package Typableitung_Inferenz_Wildcard;

/**
 * Kopiert aus LE04_5A5G
 */
public class Util<T> {
    /**
     * Sinnvoll ergaenzt
     * 
     * @param <T>
     * @param v1
     * @param v2
     * @return
     */
    public static <T> T zufall(T v1, T v2) {
        return Math.random() < 0.5 ? v1 : v2;
    }
}