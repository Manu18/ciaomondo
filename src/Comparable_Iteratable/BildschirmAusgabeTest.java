package Comparable_Iteratable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

// add imports you need for your Tests

public class BildschirmAusgabeTest { // give your Testclass an appropiate name

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream(); //stream to save the sreen output to
	private final PrintStream original = System.out; //the original stream for the screen output

	@BeforeEach
	public void setupStream(){ //should be self explanatory
		System.setOut(new PrintStream(outContent)); 
	}

	@AfterEach
	public void resetStream(){ //should be self explanatory
		System.setOut(original);
	}
	
	//---------------------------------------------------------------------
	// write your own Tests!
	//---------------------------------------------------------------------

	@Test
	public void testHelloWorld() {
		System.out.print("Hello World");
		assertEquals("Hello World", outContent.toString());
	} 

}
