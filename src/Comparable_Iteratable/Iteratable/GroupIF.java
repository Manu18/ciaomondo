package Comparable_Iteratable.Iteratable;

import GenerischeDaten.TableSpaceOutOfBoundsException;

import java.util.Iterator;

public interface GroupIF<T> extends Iterable {
    int size();
    boolean isEmpty();
    void appendLast(T g) throws TableSpaceOutOfBoundsException;
    T removeLast();
    T get(int p);
    void swap(int p1, int p2);
    Iterator<T> iterator();
}
