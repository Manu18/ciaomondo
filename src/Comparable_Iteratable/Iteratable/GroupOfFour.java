package Comparable_Iteratable.Iteratable;

import GenerischeDaten.TableSpaceOutOfBoundsException;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class GroupOfFour<T> implements GroupIF<T> {
    private int size = 0;
    protected T ersterPlatz;
    protected T zweiterPlatz;
    private T dritterPlatz;
    private T vierterPlatz;


    public GroupOfFour() {
    }

    public GroupOfFour(T o1) throws TableSpaceOutOfBoundsException {
        this.appendLast(o1);
    }

    public GroupOfFour(T o1, T o2) throws TableSpaceOutOfBoundsException {
        this.appendLast(o1);
        this.appendLast(o2);
    }

    public GroupOfFour(T o1, T o2, T o3) throws TableSpaceOutOfBoundsException {
        this.appendLast(o1);
        this.appendLast(o2);
        this.appendLast(o3);
    }

    public GroupOfFour(T o1, T o2, T o3, T o4) throws TableSpaceOutOfBoundsException {
        this.appendLast(o1);
        this.appendLast(o2);
        this.appendLast(o3);
        this.appendLast(o4);
    }
    public GroupOfFour(T[] array) throws TableSpaceOutOfBoundsException {
        for (int i = 0; i < 4 && i < array.length; i++) {
            this.appendLast(array[i]);
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void appendLast(T g) throws TableSpaceOutOfBoundsException {
        switch (size()) {
            case 0:
                ersterPlatz = g;
                size++;
                break;
            case 1:
                zweiterPlatz = g;
                size++;
                break;
            case 2:
                dritterPlatz = g;
                size++;
                break;
            case 3:
                vierterPlatz = g;
                size++;
                break;
            case 4:
                throw new TableSpaceOutOfBoundsException("Kein Platz mehr am Tisch");
        }
    }

    public T removeLast() {
        T retG = null;
        switch (size()) {
            case 0:
                 throw new NullPointerException("Speicher ist leer, keine Gerichte gespeichert");
            case 1:
                size--;
                retG = ersterPlatz;
                ersterPlatz = null;
                break;
            case 2:
                size--;
                retG = zweiterPlatz;
                zweiterPlatz = null;
                break;
            case 3:
                size--;
                retG = dritterPlatz;
                dritterPlatz = null;
                break;
            case 4:
                size--;
                retG = vierterPlatz;
                vierterPlatz = null;
                break;
        }
        return retG;
    }

    public T get(int p) {
        switch (p) {
            case 0:
                throw new IndexOutOfBoundsException("Kein Platz mit dem Index vorhanden");
            case 1:
                return ersterPlatz;
            case 2:
                return zweiterPlatz;
            case 3:
                return dritterPlatz;
            case 4:
                return vierterPlatz;
        }
        throw new IndexOutOfBoundsException("Kein Platz mit dem Index vorhanden");
    }

    public void swap(int p1, int p2) {
        T c1 = this.get(p1);
        T c2 = this.get(p2);

        if (c1 == null | c2 == null) {
            throw new NullPointerException("Swap mit leerem Objekt nicht möglich");
        }

        switch (p1) {
            case 1:
                this.ersterPlatz = c2;
            case 2:
                this.zweiterPlatz = c2;
            case 3:
                this.dritterPlatz = c2;
            case 4:
                this.vierterPlatz= c2;
        }
        switch (p2) {
            case 1:
                this.ersterPlatz = c1;
            case 2:
                this.zweiterPlatz = c1;
            case 3:
                this.dritterPlatz = c1;
            case 4:
                this.vierterPlatz = c1;
        }
    }

    public void clear() {
        ersterPlatz = null;
        zweiterPlatz = null;
        dritterPlatz = null;
        vierterPlatz = null;
    }

    public String toString() {
        String s = "Tisch mit " + size() + " Personen: \n";
        if (size() > 0)
            s += "Platz 1 hat bestellt: " + ersterPlatz.toString();
        if (size() > 1)
            s += "Platz 2 hat bestellt: " + zweiterPlatz.toString();
        if (size() > 2)
            s += "Platz 3 hat bestellt: " + dritterPlatz.toString();
        if (size() > 3)
            s += "Platz 4 hat bestellt: " + vierterPlatz.toString();
        return s;
    }

    // A2
    @Override
    public Iterator<T> iterator() {
        return new GroupOFFourIterator<T>(this);
    }

    // Iterator
    private class GroupOFFourIterator<T> implements Iterator<T> {
        private int i;
        private GroupOfFour<T> g;

        public GroupOFFourIterator(GroupOfFour<T> g) {
            this.i = 0;
            this.g = g;
        }

        @Override
        public boolean hasNext() {
            return i < g.size();
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            } else {
                return g.get(++i);
            }
        }
    }
}
