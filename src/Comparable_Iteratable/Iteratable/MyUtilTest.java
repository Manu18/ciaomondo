package Comparable_Iteratable.Iteratable;

import Comparable_Iteratable.Comparable.Gericht;
import Comparable_Iteratable.Comparable.Salat;
import GenerischeDaten.TableSpaceOutOfBoundsException;
import org.junit.jupiter.api.Test;

import static Comparable_Iteratable.Iteratable.MyUtil.shallowCopy;
import static org.junit.jupiter.api.Assertions.*;

public class MyUtilTest {

    @Test
    void test() {
        GroupIF<Integer> g1 = new GroupOfFour<>();
        GroupIF<Integer> g2 = new GroupOfFour<>();
        try {
            g1.appendLast(1);
            g2.appendLast(2);
        } catch (GenerischeDaten.TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }
        assertThrows(TableSpaceOutOfBoundsException.class, () -> shallowCopy(g1,g2), "Fehler wird nicht geworfen");

        GroupIF<Salat> salatGroupIF = new GroupOfFour<>();
        try {
            salatGroupIF.appendLast(new Salat(1,2));
        } catch (TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }
        GroupIF<Gericht> gerichtGroupIF = new GroupOfFour<>();
        // shallowCopy(salatGroupIF, gerichtGroupIF);
        // Wird nicht angenommen
        //assertEquals(salatGroupIF, gerichtGroupIF, "Salat kann nicht in Gericht kopiert werden");
    }

    // Testen Sie auch, ob Sie beispielsweise ein GroupIF<Salat> in ein GroupIF<Gericht> kopieren koennen.

    // Warum koennte man keine sogenannte deepcopy machen bei der nicht die Referenzen, sondern die Inhalte der Objekte kopiert werden?
    /*
        deepcopy von unterschiedlichen Objekttypen ist nicht moeglich, da deren Speicherstruktur natuerlich unterschiedlich ist und sie nicht
        notwendigerweise den gleichen Platz haben, um die Inhalte aufzunehmen.
     */
}
