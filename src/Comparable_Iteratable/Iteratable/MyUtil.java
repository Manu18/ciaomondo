package Comparable_Iteratable.Iteratable;

import Comparable_Iteratable.Comparable.Gericht;
import GenerischeDaten.Salat;
import GenerischeDaten.TableSpaceOutOfBoundsException;

public class MyUtil {
    public static void main(String[] args) throws TableSpaceOutOfBoundsException {
        // A1
        GroupOfFour<Integer> gi = new GroupOfFour<>();
        int[] arr = {1,2,3};
        for (int element : arr) {
            element = 42;
        }
        /*
            Ein GroupOfFour<Integer> Objekt kann nicht in der foreach schleife verwendet werden, solange es nicht Iteratable implementiert hat.
            Außerdem funktioniert eine Zuweisung nicht, wir koennen einen Iterator nutzen, der die Plaetze mit den Integern zurueckgibt, aber
            durch element = 42 wird der Platz nicht auf 42 gesetzt, denn das wuerde nur ueber gi.appendLast() und gi.removeLast() funktionieren.
            Die foreach Schleife gibt bei der aktuellen Implementation das jeweilige Objekt zurück, also die Referenz.

            //// Aber nicht sicher, ob das so stimmt...
         */

        GroupOfFour<Gericht> quelle = new GroupOfFour<>();
        GroupOfFour<Gericht> ziel = new GroupOfFour<>();
        Gericht g1 = new Gericht("test",123);
        Gericht g2 = new Gericht("test1",123);
        Gericht g3 = new Gericht("test2",123);
        Salat s1 = new Salat(10,123);
        Salat s2 = new Salat(10,123);
        Salat s3 = new Salat(10,123);
        quelle.appendLast(g1);
        quelle.appendLast(g2);
        quelle.appendLast(g3);
        quelle.appendLast(g3);

        printAll(quelle);
        System.out.println("hallo");
        shallowCopy(quelle,ziel);
        printAll(ziel);
    }

    // A3
    public static <T> void printAll(GroupIF<T> ig) {
        for(Object s : ig) {
            System.out.println(s);
        }
    }

    public static <T> void printAll(GroupIF<T> ig1, GroupIF<T> ig2) {
        for (Object o1 : ig1) {
            System.out.println("1: " + o1);
        }
        for (Object o2 : ig2) {
            System.out.println("2: " + o2);
        }
    }

    // A4
    public static <T> void shallowCopy(GroupIF<T> quelle, GroupIF<T> ziel) throws TableSpaceOutOfBoundsException {
        if (!ziel.isEmpty()) {
            throw new TableSpaceOutOfBoundsException();
        }
        for (Object ref : quelle) {
            ziel.appendLast((T) ref);
        }
    }

}
