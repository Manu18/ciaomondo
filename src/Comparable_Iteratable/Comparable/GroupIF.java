package Comparable_Iteratable.Comparable;

import GenerischeDaten.TableSpaceOutOfBoundsException;

public interface GroupIF<T> extends Comparable<T> {
    int size();
    boolean isEmpty();
    void appendLast(T g) throws TableSpaceOutOfBoundsException;
    T removeLast();
    T get(int p);
    void swap(int p1, int p2);
    int compareTo(T other);
}
