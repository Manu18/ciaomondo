package Comparable_Iteratable.Comparable;

public class Gericht implements Comparable<Gericht> {
    private String course = "";
    private int price = 0;

    public Gericht() {
        this.course = "Bestseller";
        this.price = 18;
    }

    public Gericht(String s, int i) {
        this.course = s;
        this.price = i;
    }

    public String getCourse() {
        return course;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return "Gericht";
    }

    // A1
    @Override
    public int compareTo(Gericht t) {
        int c = this.getCourse().compareTo(t.getCourse());
        if (c == 0) {
            c = this.getPrice() - t.getPrice();
        }
        return c;
    }

    // A2
    /*
    Erlauutern Sie mit eigenen Worten moegliche Probleme/Nachteile,
    die sich aus der vorherigen Aufgabe (Comparable<Gericht>) fuer unser Softwaredesign (Gericht, Salat, Pizza) ergeben.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    - Das Problem ist nun, dass wir die Sicht auf ein Gericht einschraenken. Eigene Merkmale der abgeleiteten Klassen von Gericht
    koennen nicht verglichen werden.
    - Wir koennen nur Gerichte vergleichen, andere Objekte koennen nicht mit einem Gericht verglichen werden.
    - In den speziellen Fall muss auch course entsprechend Formatiert werden, was nicht so natuerlich ist, aber machbar.
     */

}
