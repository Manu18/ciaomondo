package Comparable_Iteratable.Comparable;

import java.util.Comparator;


class Generic<T> {

}

public class MyUtil {
    // A3
    public static <T extends Comparable<T>> T max(GroupIF<T> groupIF) {
        if (groupIF != null) {
            T maxE = groupIF.get(1);
            for (int i = 2; i <= groupIF.size(); i++) {
                maxE = groupIF.get(i).compareTo(maxE) > 0 ? groupIF.get(i) : maxE;
            }
            return maxE;
        } else {
            return null;
        }
    }

    public static <T> T max(GroupIF<T> groupIF, Comparator<? super T> comparator) {
        if (groupIF != null) {
            T maxE = groupIF.get(1);
            T erg = null;
            for (int i = 2; i <= groupIF.size(); i++) {
                maxE = comparator.compare(groupIF.get(i), maxE) > 0 ? groupIF.get(i) : maxE;
            }
            return maxE;
        } else {
            return null;
        }
    }

}
