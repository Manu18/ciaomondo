package Comparable_Iteratable.Comparable;

public class Salat extends Gericht {
    private int weight = 0;

    public Salat() {
        super("1 - Vorspeise",5);
    }

    public Salat(int p, int w) {
        super("1 - Vorspeise",p);
        this.weight = w;
    }

    public int getWeight() {
        return weight;
    }

    public String toString() {
        return "Salat mit Gewicht von " + getWeight();
    }

    // A2
    public int compareTo(Salat t) {
        int c = this.getWeight() - t.getWeight();
        return c;
    }

}
