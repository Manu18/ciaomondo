package Comparable_Iteratable.Comparable;

import Comparator_Iterator.Comparator.ComparatorGerichtPrice;
import GenerischeDaten.TableSpaceOutOfBoundsException;
import org.junit.jupiter.api.Test;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

public class MyUtilTest {
    @Test
    void testMax1() {
        // GroupIF mmit verschidenen Typen
        // public static <T extends Comparable<T>> T max(GroupIF<T> groupIF) {

        GroupIF<Gericht> gerichtGroupIF = new GroupOfFour<>();
        Gericht pizza = new Pizza(1122,2);
        try {
            gerichtGroupIF.appendLast(new Gericht("adsf",12));
            gerichtGroupIF.appendLast(new Gericht("asdafqew",15));
            gerichtGroupIF.appendLast(new Salat(122,1));
            gerichtGroupIF.appendLast(pizza);
        } catch (TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }

        GroupIF<Integer> integerGroupIF = new GroupOfFour<Integer>();
        Integer n = 3;
        try {
            integerGroupIF.appendLast(1);
            integerGroupIF.appendLast(2);
            integerGroupIF.appendLast(n);
        } catch (TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }

        assertEquals(n, MyUtil.<Integer>max(integerGroupIF),"Groesstes Element nicht zurueckgegeben");
        //assertEquals(pizza, MyUtil.<Gericht>max(gerichtGroupIF),"Groesstes Element nicht zurueckgegeben");
        // compareTo vergleicht hier String hashes, dh. deaktiviert.

    }

    @Test
    void testMax2() {
        GroupIF<Gericht> gerichtGroupIF = new GroupOfFour<>();
        Pizza pizza = new Pizza(1122,2);
        try {
            gerichtGroupIF.appendLast(new Gericht("adsf",12));
            gerichtGroupIF.appendLast(new Gericht("asdafqew",15));
            gerichtGroupIF.appendLast(new Salat(12,1));
            gerichtGroupIF.appendLast(pizza);
        } catch (TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }

        GroupIF<Integer> integerGroupIF = new GroupOfFour<Integer>();
        Integer n = 12312123;
        try {
            integerGroupIF.appendLast(12);
            integerGroupIF.appendLast(1123);
            integerGroupIF.appendLast(n);
        } catch (TableSpaceOutOfBoundsException e) {
            e.printStackTrace();
        }

        Comparator<Gericht> gerichtComparator = new Comparator<Gericht>() {
            @Override
            public int compare(Gericht t0, Gericht t1) {
                return t0.getPrice() - t1.getPrice();
            }
        };
        Comparator<Integer> integerComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer t0, Integer t1) {
                return t0 - t1;
            }
        };

        assertEquals(n, MyUtil.<Integer>max(integerGroupIF, integerComparator),"Groesstes Element nicht zurueckgegeben");
        assertEquals(pizza, MyUtil.<Gericht>max(gerichtGroupIF, gerichtComparator),"Groesstes Element nicht zurueckgegeben");
    }
}
