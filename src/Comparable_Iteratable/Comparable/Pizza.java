package Comparable_Iteratable.Comparable;

public class Pizza extends Gericht {
    private int diameter = 0;

    public Pizza() {
        super("2 - Hauptspeise",10);
    }

    public Pizza(int p, int d) {
        super("2 - Hauptspeise",p);
        this.diameter = d;
    }

    public int getDiameter() {
        return this.diameter;
    }

    public String toString() {
        return "Pizza mit Durchmesser von " + getDiameter();
    }

    // A2
    public int compareTo(Pizza t) {
        int c = this.getDiameter() - t.getDiameter();
        return c;
    }
}
