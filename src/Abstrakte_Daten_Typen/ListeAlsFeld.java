package Abstrakte_Daten_Typen;

import java.util.Comparator;
import java.util.Iterator;

public class ListeAlsFeld<T> implements Liste<T> {
    static int maxSize;
    private int size;
    T data[];

    public ListeAlsFeld() {
        this(10);
    }
    @SuppressWarnings("unchecked")
    public ListeAlsFeld(int m) {
        maxSize = m;
        data = (T[]) new Object[maxSize];
    }

    @Override
    public boolean add(int index, T t) {
        if (index < size) {
            // ueberschreiben
            data[index] = t;
        } else if (index < maxSize) {
            data[index] = t;
            size = index + 1;
        } else {
            throw new IndexOutOfBoundsException();
            // return false;
        }
        return true;
    }

    @Override
    public T get(int index) {
        if (index < maxSize) {
            if (index < size) {
                return data[index];
            } else {
                throw new NullPointerException();
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public T remove(int index) {
        T cache = data[index];
        data[index] = null;
        if (index == (size - 1)) {
            size--;
        }
        return cache;
    }

    @Override
    public boolean add(T t) {
        return add(size++, t);
    }

    @Override
    public boolean remove(T t) {
        for (int i = 0; i < size; i++) {
            if (t.equals(data[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return (0 == size);
    }

    @Override
    public int size() {
        return size;
    }

    public int size2() {
        int c = 0;
        for (int i = 0; i < data.length; i++) {
            c++;
        }
        return c;
    }

    // optional
    public void sort(Comparator<? super T> c) {
        int sortiert = 0;
        while (sortiert <= (size - 2)) {
            sortiert = 0;
            for (int i = 0; i < (size - 1); i++) {
                if (c.compare(data[i], data[i+1]) < 0) {
                    T cache = data[i];
                    data[i] = data[i+1];
                    data[i+1] = cache;
                    sortiert = 0;
                } else {
                    sortiert++;
                }
            }
        }
    }

    public static void main(String[] args) {
        ListeAlsFeld<String> list = new ListeAlsFeld<>(10);
        list.add("a");
        list.add("c");
        list.add("b");
        list.add("a");
        list.add("z");
        list.add("f");
        Alphabetic_Comparator comparator = new Alphabetic_Comparator();
        list.sort(comparator);
    }
}
