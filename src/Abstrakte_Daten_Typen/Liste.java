package Abstrakte_Daten_Typen;

import java.util.Comparator;

public interface Liste<T> extends Kollektion<T> {
    boolean add(int index, T t);
    T get(int index); // throws IndexOutOfBoundsException
    T remove(int index); // throws IndexOutOfBoundsException

    // optional fuer doppelt verkettete Listen:
    // T peek(); // throws NoSuchElementException

    // optional
    void sort(Comparator<? super T> c);
}
