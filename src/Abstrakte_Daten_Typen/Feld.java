package Abstrakte_Daten_Typen;

public class Feld<T> implements Kollektion<T>{
    static int maxSize;
    private int size;
    T data[];

    public Feld(int m) {
        maxSize = m;
        data = (T[]) new Object[maxSize];
    }

    @Override
    public boolean add(T t) {
        if (size < maxSize) {
            data[size++] = t;
            return true;
        }
        else return false;
    }

    @Override
    public boolean remove(T t) {
        int pos = 0;
        while (pos < size) {
            if (data[pos].equals(t)) {
                data[pos] = null;
                while(pos < (size - 1)) {
                    data[pos] = data[++pos];
                }
                size--;
                return true;
            } else {
                pos++;
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return (0 == size);
    }

    @Override
    public int size() {
        return size;
    }
}
