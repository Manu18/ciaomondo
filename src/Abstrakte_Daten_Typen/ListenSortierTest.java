package Abstrakte_Daten_Typen;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ListenSortierTest {
    @Test
    void listTest() {
        ListeAlsFeld<String> list = new ListeAlsFeld<>(10);
        list.add("Hi");
        assertEquals("Hi", list.remove(0), "Falsche Rückgabe beim Löschen");
        list.add("Hi");
        list.add("Hallo");
        list.add(3,"World");
        list.add(5, "Ciao");
        assertEquals("Hallo", list.remove(1), "Falsche Rückgabe beim Löschen");
        assertEquals(true, list.remove("Hi"), "Falsche Rückgabe beim Löschen");
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(10,"Test"), "Keine Fehlermeldung");
        assertEquals("World", list.get(3), "Get funktioniert nicht");
    }

    @Test
    void sortTest() {
        ListeAlsFeld<String> list = new ListeAlsFeld<>(10);
        list.add("a");
        list.add("c");
        list.add("b");
        list.add("a");
        list.add("z");
        list.add("f");
        Alphabetic_Comparator comparator = new Alphabetic_Comparator();
        list.sort(comparator);

        assertEquals("a" ,list.get(0), "Sortier Vorgang fehlgeschlagen");
        assertEquals("a" ,list.get(1), "Sortier Vorgang fehlgeschlagen");
        assertEquals("b" ,list.get(2), "Sortier Vorgang fehlgeschlagen");
        assertEquals("c" ,list.get(3), "Sortier Vorgang fehlgeschlagen");
        assertEquals("f" ,list.get(4), "Sortier Vorgang fehlgeschlagen");
        assertEquals("z" ,list.get(5), "Sortier Vorgang fehlgeschlagen");

    }

}
