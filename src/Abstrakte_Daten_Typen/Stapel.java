package Abstrakte_Daten_Typen;

public interface Stapel<T> extends Liste<T> {

    // alle Methoden von Vektor

    T peek();
    T pop(); // throws EmptyStackException
    T push(T t);
}
