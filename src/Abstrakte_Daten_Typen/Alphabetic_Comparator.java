package Abstrakte_Daten_Typen;

import java.util.Comparator;

public class Alphabetic_Comparator implements Comparator<String> {

    public int compare(String o1, String o2) {
        // inoriere gross klein
        o1 = o1.toLowerCase();
        o2 = o2.toLowerCase();
        for (int i = 0; i < o1.length() && i < o2.length(); i++) {
            if (o1.charAt(i) == o2.charAt(i)) {
               // next check
            } else {
                return o2.charAt(i) - o1.charAt(i);
            }
        }

        if (o1.length() == 0) return -1;
        if (o2.length() == 0) return 1;
        return 0;
    }
}
