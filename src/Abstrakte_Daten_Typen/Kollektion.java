package Abstrakte_Daten_Typen;

public interface Kollektion<T> {
    boolean add(T t);
    boolean remove(T t);
    boolean isEmpty();
    int size();
}
