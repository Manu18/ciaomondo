package Abstrakte_Daten_Typen;

public abstract class Schlange<E> implements Kollektion<E> {
    E first;
    E last;
    int size;

    class LE {
        E data;
        LE next;
        LE prev;

        LE(E e) {
            data = e;
            next = null;
        }
    }

    // Kollektion
    public abstract boolean add(E t);
    public abstract boolean remove(E t);
    public abstract boolean isEmpty();
    public abstract int size();

    public abstract E peek();
    public abstract E remove();
}
